//
//  OLISticker.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OLISticker : NSObject
{
	int stickerId;
	NSString *stickerName;
	NSString *stickerType;
	NSData *stickerData;
	float stickerWidth;
	float stickerHeight;
}

@property( assign ) int stickerId;
@property( nonatomic, retain ) NSString *stickerName;
@property( nonatomic, retain ) NSString *stickerType;
@property( nonatomic, retain ) NSData *stickerData;
@property( assign ) float stickerWidth;
@property( assign ) float stickerHeight;


@end