//
//  OLIControlsOverlayView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIMainMenuView.h"


@implementation OLIMainMenuView

/*********** Initilization and Construction ***********/

	//
- (id)initWithFrame:(CGRect)frame
{
		//    
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{		
			//create the button to tap to myDolls
		myDollsButton = [[UIButton alloc] initWithFrame: CGRectMake( 100, 0, 100, 100 )];
		
			//
		[myDollsButton addTarget: self action: @selector(doShowDolls:) forControlEvents: UIControlEventTouchUpInside];
		[myDollsButton setImage: [UIImage imageNamed: @"mydolls"] forState: UIControlStateNormal];
		[myDollsButton setTitle: @"My Dolls" forState: UIControlStateNormal];
		[myDollsButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];
		
			//create the button for pictures
		myPicturesButton = [[UIButton alloc] initWithFrame: CGRectMake( 200, 0, 100, 100 )];
		
			//
		[myPicturesButton addTarget: self action: @selector(doShowPictures:) forControlEvents: UIControlEventTouchUpInside];
		[myPicturesButton setImage: [UIImage imageNamed: @"mypictures"] forState: UIControlStateNormal];
		[myPicturesButton setTitle: @"My Pictures" forState: UIControlStateNormal];
		[myPicturesButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];

			//create the button for pictures
		myStoreButton = [[UIButton alloc] initWithFrame: CGRectMake( 300, 0, 100, 100 )];
		
			//
		[myStoreButton addTarget: self action: @selector(doShowStore:) forControlEvents: UIControlEventTouchUpInside];
		[myStoreButton setImage: [UIImage imageNamed: @"buyoutfits"] forState: UIControlStateNormal];
		[myStoreButton setTitle: @"My Pictures" forState: UIControlStateNormal];
		[myStoreButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];		
		
			//
		[self setBackgroundColor: [UIColor clearColor]];

			//add assets to the stage
		[self addSubview: myDollsButton];
		[self addSubview: myPicturesButton];
			//[self addSubview: myStoreButton];
    }
	
		//
    return self;
}

/*********** Button Selectors ***********/

	//
- (void) doShowDolls: (UIButton *) target
{
		//
	[self playButtonSelectSound];
	
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got selected
	[nc postNotificationName: @"OLIShowDolls" object: nil];
}

	//
- (void) doShowPictures: (UIButton *) target
{
		// 
	[self playButtonSelectSound];
	
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show pictures got selected
	[nc postNotificationName: @"OLIShowPictures" object: nil];
}

	//
- (void) doShowStore: (UIButton *) target
{
		// 
	[self playButtonSelectSound];
	
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show pictures got selected
	[nc postNotificationName: @"OLIShowStore" object: nil];
}

/*********** Sounds ***********/

	//
- (void) playButtonSelectSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"Clook-Public_D-9" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);		
}


/*********** Deconstruction ***********/

	//
- (void) dealloc
{
		//
	[myDollsButton release];
	[myPicturesButton release];
	[myStoreButton release];
	
		//
    [super dealloc];
}


@end
