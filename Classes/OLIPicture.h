//
//  OLIPicture.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OLIPicture : NSObject
{
	int pictureId;
	NSData *pictureData;
}

@property( assign ) int pictureId;
@property( nonatomic, retain ) NSData *pictureData;


@end
