//
//  OLIPictureDesigner.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>
#import "OLIPicture.h"
#import "OLIPictureView.h"
#import "OLIPicturesMenuView.h"
#import "OLIStickerOverlayScrollView.h"


@interface OLIPictureDesigner : UIView <UIPrintInteractionControllerDelegate, MFMailComposeViewControllerDelegate>
{
		//
	CGPoint first;
	
		//stores all of the data
	NSDictionary *stickers;
	NSMutableArray *stickerViews;
	
		//
	OLIPicturesMenuView *menuView;
	OLIPictureView *pictureView;
	
		//
	UIPopoverController *stickerPopOverController;
	UIView *stickerDrawOverlay;
	UIView *pictureContent;
	
		//
	OLISticker *currentSticker;
	OLIStickerView *stickerViewToEdit;
	
		//
	float toEditScale;
	float toEditRotation;
	CGPoint toEditPoint;
	
		//
    UIViewController *parentController;
	
		//
	UITapGestureRecognizer *tapGesture;
	UIRotationGestureRecognizer *rotateGesture;
	UIPinchGestureRecognizer *pinchGesture;
	UIPanGestureRecognizer *panGesture;
}

	// Don't use retain or you'll have a circular reference
@property(nonatomic, assign) UIViewController *parentController;

- (void) displayWithPicture: (OLIPicture *) thePicture andStickers: (NSDictionary *) stickerData;

	//
- (void) playAddStickerSound;
- (void) playRemoveStickerSound;
- (void) playButtonSelectSound;

	//
- (void) animateStickerAdd: (OLIStickerView *) stickerToAdd;
- (void) animateStickerRemove: (OLIStickerView *) stickerToRemove;


@end
