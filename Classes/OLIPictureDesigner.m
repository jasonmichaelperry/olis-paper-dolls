//
//  OLIPictureDesigner.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPictureDesigner.h"


@implementation OLIPictureDesigner

@synthesize parentController;

- (id)initWithFrame:(CGRect)frame
{

		//
    self = [super initWithFrame:frame];

		//
	if (self)
	{
			//
		currentSticker = nil;
		stickerViewToEdit = nil;
		
			//
		stickerViews = [[NSMutableArray alloc] init];
		
			//listen for the XML data to load
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//
		[nc addObserver:self selector: @selector(doShowStickers:) name:@"OLIShowStickers" object:nil];
		[nc addObserver:self selector: @selector(doSharePicture:) name:@"OLIPictureShare" object:nil];
		[nc addObserver:self selector: @selector(doPrintPicture:) name:@"OLIPicturePrint" object:nil];
		[nc addObserver:self selector: @selector(doSelectedSticker:) name:@"OLISelectedSticker" object:nil];	
		
			//creates a gesture to select a doll
		tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(doStickerTap:)];
		tapGesture.numberOfTouchesRequired = 1;	//number of taps needed for a touch	
		
			//
		panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(doStickerMove:)];
		panGesture.maximumNumberOfTouches = 1;
		
			//
		rotateGesture = [[UIRotationGestureRecognizer alloc] initWithTarget: self action: @selector(doStickerRotate:)];
		
			//
		pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget: self action: @selector(doStickerPinch:)];
		[pinchGesture setDelaysTouchesBegan: YES];
		
			//
		stickerDrawOverlay = [[UIView alloc] initWithFrame: CGRectMake( 0, 0, 1024, 768)];
		
			//
		[stickerDrawOverlay addGestureRecognizer: tapGesture];
		[stickerDrawOverlay addGestureRecognizer: panGesture];
		[stickerDrawOverlay addGestureRecognizer: pinchGesture];
		[stickerDrawOverlay addGestureRecognizer: rotateGesture];
		
			//
		pictureContent = [[UIView alloc] initWithFrame: CGRectMake( 0, 0, 1024, 768)];

			//
		[pictureContent addSubview: stickerDrawOverlay];		
		
			//
		menuView = [[OLIPicturesMenuView alloc] initWithFrame: CGRectMake(0, 650, 768, 100)];
		
			//
		[self addSubview: pictureContent];
		[self addSubview: menuView];
	}
	
		//
    return self;
}

- (void) displayWithPicture: (OLIPicture *) thePicture andStickers: (NSDictionary *) stickerData
{	
		//
	stickers = [stickerData retain];
	
		//create a model instance of the doll
	pictureView = [[OLIPictureView alloc] initWithFrame: CGRectMake( 0, 0, 1024, 768)];
	
		//display the doll selected
	[pictureView setPicture: thePicture];
	
		//populate it into the display
	[pictureContent insertSubview: pictureView belowSubview: stickerDrawOverlay];
	
}

/*********** Button and Notification Targets ***********/

	//
- (void) doClearStickers
{
		//
	if([stickerPopOverController isPopoverVisible])
	{
			//
		[stickerPopOverController dismissPopoverAnimated:YES];
	}
	
		//step through all the stickers
	for( OLIStickerView *stickerItemView in stickerViews )
	{
			//remove the sticker from stage
		[stickerItemView removeFromSuperview];		
	}

		//
	[stickerViews removeAllObjects];
	
		//
	[stickerViews release];
	
		//
	stickerViews = [[NSMutableArray alloc] init];
	
}

	//
-(void) doShowStickers: (NSNotification *) note
{
		//
	if([stickerPopOverController isPopoverVisible])
	{
			//
		[stickerPopOverController dismissPopoverAnimated:YES];
	}
	
		//
	UIButton *sender = (UIButton *)[note object];
	
		//
	NSString *stickerType = [sender titleForState: UIControlStateNormal];
	
		//
	CGRect anchor = [sender frame];
	
		//
	OLIStickerOverlayScrollView *stickerOverlayView = [[OLIStickerOverlayScrollView alloc] initWithFrame: CGRectMake( 0, 0, 600, 220)];
	
		//
	[stickerOverlayView displayStickersWithData: [stickers objectForKey: stickerType] forCategory: stickerType];		

		//build our custom popover view
	UIViewController *stickerContentController = [[UIViewController alloc] init];
	stickerContentController.view = stickerOverlayView;
	
		//resize the popover view shown
		//in the current view to the view's size
	stickerContentController.contentSizeForViewInPopover = CGSizeMake( 600, 220);
	
		//create a popover controller
	stickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:stickerContentController];
		
		//
	[stickerPopOverController setPassthroughViews: [NSArray arrayWithObject: menuView] ];
	
		//present the popover view non-modal with a
		//refrence to the button pressed within the current view
	[stickerPopOverController presentPopoverFromRect: anchor inView: menuView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];		
	
		//release the popover content
	[stickerOverlayView release];
	[stickerContentController release];	
}

	//
- (void) doSharePicture: (NSNotification *) note
{
		//
	if([stickerPopOverController isPopoverVisible])
	{
			//
		[stickerPopOverController dismissPopoverAnimated:YES];
	}
	
		//
	UIGraphicsBeginImageContext(pictureContent.bounds.size);
	
		//
	[pictureContent.layer renderInContext:UIGraphicsGetCurrentContext()];
	
		//
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
		//
	MFMailComposeViewController *email = [[MFMailComposeViewController alloc] init];

		//
	[email setMailComposeDelegate: self];
	[email setSubject: @"I Made You an Oli Paper Doll"];
	[email setMessageBody: @"" isHTML:NO];
	[email addAttachmentData:  UIImagePNGRepresentation(viewImage)  mimeType: @"image/png" fileName: @"myOliPaperDollPicture"];
	
	 //
	[parentController presentModalViewController: email animated: YES];

	 //
	 [email release];
}

	//
- (void) doPrintPicture: (NSNotification *) note
{
		//
	if([stickerPopOverController isPopoverVisible])
	{
			//
		[stickerPopOverController dismissPopoverAnimated:YES];
	}
	
		//
	UIGraphicsBeginImageContext(pictureContent.bounds.size);

		//
	[pictureContent.layer renderInContext:UIGraphicsGetCurrentContext()];
	
		//
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
		//print controller
	UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
	
		//
	if(pic && [UIPrintInteractionController canPrintData: UIImagePNGRepresentation(viewImage)] )
	{
			//
		pic.delegate = self;
		
			//
		UIPrintInfo *printInfo = [UIPrintInfo printInfo];
		printInfo.jobName = @"My OLI Paper Doll Picture";
		printInfo.outputType = UIPrintInfoOutputPhoto;
		printInfo.duplex = UIPrintInfoDuplexLongEdge;
		printInfo.orientation = UIPrintInfoOrientationLandscape;
		
			//
		pic.printInfo = printInfo;
		pic.showsPageRange = YES;
		pic.printingItem = UIImagePNGRepresentation(viewImage);
			
			//
		UIPrintInteractionCompletionHandler completionHandler = 
			^(UIPrintInteractionController *printController, BOOL completed, NSError *error)
			{
				if(!completed && error)
				{
					NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);   
				}
			};
			
			//
		[pic presentFromRect: [(UIButton *)[note object] frame] inView: menuView animated: YES completionHandler: completionHandler];
			//[pic presentAnimated:YES completionHandler: completionHandler];
	}
}

	//
- (void) doSelectedSticker: (NSNotification *) note
{
		//
	if([stickerPopOverController isPopoverVisible])
	{
			//
		[stickerPopOverController dismissPopoverAnimated:YES];
	}
	
		//new sticker selected so release the editor view
	stickerViewToEdit = nil;
	
		//store the selected sticker for adding later
	currentSticker = [note object];
}

/*********** Gesture Selectors ***********/

	//
- (void) doStickerTap: (UITapGestureRecognizer *) sender
{
		//originial tap location
	CGPoint tapPoint = [sender locationInView: stickerDrawOverlay];
	
		//
	OLIStickerView *selectedSticker = nil;
	
		//step through each doll for the selected one
	for( OLIStickerView *stickerItemView in stickerViews )
	{
			//used to determine if is in bounds
		CGFloat xMin = stickerItemView.frame.origin.x;
		CGFloat xMax = stickerItemView.frame.origin.x + stickerItemView.frame.size.width;
		CGFloat yMin = stickerItemView.frame.origin.y;
		CGFloat yMax = stickerItemView.frame.origin.y + stickerItemView.frame.size.height;
		
			//it is within
		if( tapPoint.x > xMin && tapPoint.x < xMax && tapPoint.y > yMin && tapPoint.y < yMax )
		{
				//
				//[stickerViewToEdit setAlpha: 1];
			
				//
			stickerViewToEdit = nil;
			
				//get the paper doll that was selected
			selectedSticker = stickerItemView;
			
				//no need to keep looping.  We got it.
			break;
		}
	}
	
		//if a sticker was selected remove it?
	if( selectedSticker != nil && menuView.isEraseMode )
	{
			//
		[stickerViews removeObject: selectedSticker];
			
			//remove this sticker
		[self animateStickerRemove: selectedSticker];
	}
	
		//no sticker selected so add it
	else if( !menuView.isEraseMode && currentSticker != nil )
	{
			//create a new sticker instance
		OLIStickerView *newSticker = [[OLIStickerView alloc] initWithFrame: CGRectMake( tapPoint.x - ([currentSticker stickerWidth]/2), tapPoint.y - ([currentSticker stickerHeight]/2), [currentSticker stickerWidth], [currentSticker stickerHeight])];
		
			//current sticker type to display
		[newSticker setSticker: currentSticker];
		
			//
		[stickerViews addObject: newSticker];
		
			//
		[stickerDrawOverlay addSubview: newSticker];
		
			//
		[self animateStickerAdd: newSticker];
		
			//
		toEditScale = 1;
		toEditRotation = 0;
		
			//
		stickerViewToEdit = newSticker;

			//
		[stickerDrawOverlay bringSubviewToFront: stickerViewToEdit];
		
			//
		currentSticker = nil;
	}
	
		//
	else if( !menuView.isEraseMode && selectedSticker != nil && currentSticker == nil )
	{
		if( [[stickerViewToEdit sticker] stickerId] ==  [[selectedSticker sticker] stickerId] )
			return;
		
			//
			//toEditScale = 1;
			//toEditRotation = 0;

		
			//
		stickerViewToEdit = selectedSticker;
		
		
			//
		[self playButtonSelectSound];
			//
			//[stickerViewToEdit setAlpha: 0.8];
		
			//
		[stickerDrawOverlay bringSubviewToFront: stickerViewToEdit];
	}
	
}

- (void) doStickerMove: (UIPanGestureRecognizer *) sender
{
		//
	if( stickerViewToEdit == nil )
		return; 

		//
	BOOL doAnimation = NO;
	
		//
	toEditPoint = [sender translationInView: stickerDrawOverlay];
	CGPoint tapLocation = [sender locationInView: stickerDrawOverlay];
		 
		//used to determine if is in bounds
	CGFloat xMin = stickerViewToEdit.frame.origin.x; 
	CGFloat xMax = stickerViewToEdit.frame.origin.x + stickerViewToEdit.frame.size.width;
	CGFloat yMin = stickerViewToEdit.frame.origin.y;
	CGFloat yMax = stickerViewToEdit.frame.origin.y + stickerViewToEdit.frame.size.height;	

			//is the touch inside of it?
	if( tapLocation.x > xMin && tapLocation.x < xMax && tapLocation.y > yMin && tapLocation.y < yMax )
		doAnimation = YES;		
	
		//do not animate this thing 
	if( doAnimation == NO )
		return;

		// 
	if( [sender state] == UIGestureRecognizerStateBegan ) 
		first = CGPointMake(0, 0);		
	
	NSLog( @"%f,%f", toEditPoint.x, toEditPoint.y );
	
		//
		//toEditPoint = CGPointMake( first.x + toEditPoint.x, first.y + toEditPoint.y );
		//toEditPoint = CGPointMake( toEditPoint.x - (), toEditPoint.y -  );
	
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .025];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionNone];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doStickerMoveComplete:)];
	
		//
	[stickerViewToEdit setTransform: CGAffineTransformMakeTranslation(toEditPoint.x, toEditPoint.y )];
		//[stickerViewToEdit setTransform: CGAffineTransformTranslate( stickerViewToEdit.transform, toEditPoint.x, toEditPoint.y )];
	[stickerViewToEdit setTransform: CGAffineTransformRotate( stickerViewToEdit.transform, toEditRotation )];
	[stickerViewToEdit setTransform: CGAffineTransformScale( stickerViewToEdit.transform, toEditScale, toEditScale) ];		
	
		//do the animation
	[UIView commitAnimations];
}

	//
- (void) doStickerMoveComplete: (NSString *) animationID
{
		//
		//toEditPoint = stickerViewToEdit.frame.origin;
}

	//
- (void) doStickerRotate: (UIRotationGestureRecognizer *) sender
{
		//
	if( stickerViewToEdit == nil )
		return;
	
		//
	BOOL doAnimation = NO;
	
		//
	CGPoint tapLocation = [sender locationInView: stickerDrawOverlay];
	
		//used to determine if is in bounds
	CGFloat xMin = stickerViewToEdit.frame.origin.x;
	CGFloat xMax = stickerViewToEdit.frame.origin.x + stickerViewToEdit.frame.size.width;
	CGFloat yMin = stickerViewToEdit.frame.origin.y;
	CGFloat yMax = stickerViewToEdit.frame.origin.y + stickerViewToEdit.frame.size.height;	
	
		//is the touch inside of it?
	if( tapLocation.x > xMin && tapLocation.x < xMax && tapLocation.y > yMin && tapLocation.y < yMax )
		doAnimation = YES;		
	
		//do not animate this thing
	if( doAnimation == NO )
		return;
	
		//
	if( [sender state] == UIGestureRecognizerStateBegan ) 
		first = toEditPoint;	
	
		//
	toEditRotation = [sender rotation];
	
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .10];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionCurlUp];	
	
		//
	[stickerViewToEdit setTransform: CGAffineTransformMakeTranslation(toEditPoint.x, toEditPoint.y )];
	[stickerViewToEdit setTransform: CGAffineTransformRotate( stickerViewToEdit.transform, toEditRotation )];
	[stickerViewToEdit setTransform: CGAffineTransformScale( stickerViewToEdit.transform, toEditScale, toEditScale) ];	
	
		//do the animation
	[UIView commitAnimations];	
}

	//
- (void) doStickerPinch: (UIPinchGestureRecognizer *) sender
{
		//
	if( stickerViewToEdit == nil )
		return;
	
		//
	BOOL doAnimation = NO;
	
		//
	CGPoint tapLocation = [sender locationInView: stickerDrawOverlay];
	
		//used to determine if is in bounds
	CGFloat xMin = stickerViewToEdit.frame.origin.x;
	CGFloat xMax = stickerViewToEdit.frame.origin.x + stickerViewToEdit.frame.size.width;
	CGFloat yMin = stickerViewToEdit.frame.origin.y;
	CGFloat yMax = stickerViewToEdit.frame.origin.y + stickerViewToEdit.frame.size.height;	
	
		//is the touch inside of it?
	if( tapLocation.x > xMin && tapLocation.x < xMax && tapLocation.y > yMin && tapLocation.y < yMax )
		doAnimation = YES;		
	
		//do not animate this thing
	if( doAnimation == NO )
		return;
	
		//
	toEditScale = [sender scale];
	
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .10];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionCurlUp];	
	
		//
	[stickerViewToEdit setTransform: CGAffineTransformMakeTranslation(toEditPoint.x, toEditPoint.y )];
	[stickerViewToEdit setTransform: CGAffineTransformRotate( stickerViewToEdit.transform, toEditRotation )];
	[stickerViewToEdit setTransform: CGAffineTransformScale( stickerViewToEdit.transform, toEditScale, toEditScale) ];
	
		//do the animation
	[UIView commitAnimations];	
}

/*********** Mail Delegates ***********/

	//
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
		//get rid of the controller
	[parentController dismissModalViewControllerAnimated: YES];
}

/*********** Sounds ***********/

	//
- (void) playAddStickerSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"xiao_xia-matt_mat-2290" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);		
}

	//
- (void) playRemoveStickerSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"missed_s-Amit_Pat-1359" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);	
}

	//
- (void) playButtonSelectSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"Clook-Public_D-9" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);		
}

/*********** Animationsons ***********/


	//
- (void) animateStickerAdd: (OLIStickerView *) stickerToAdd
{
		//
	[stickerToAdd setAlpha: 0];
	
		//
	[self playAddStickerSound];
	
		//
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .10];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionCurlUp];	
	
		//
	[stickerToAdd setAlpha: 1];
	
		//
	[UIView commitAnimations];	
}

	//
- (void) animateStickerRemove: (OLIStickerView *) stickerToRemove
{
		//
	[stickerToRemove setAlpha: 1];
	
		//
	[self playRemoveStickerSound];
	
		//
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .10];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionCurlUp];	
	
		//
	[stickerToRemove setAlpha: 0];
	
		//
	[UIView commitAnimations];
	
		//
    [self performSelector: @selector(animateStickerRemoveComplete:)
			   withObject: stickerToRemove
			   afterDelay: .2
	 ];
	
}

	//
- (void) animateStickerRemoveComplete: (OLIStickerView *) stickerToRemove
{
		//
	[stickerToRemove removeFromSuperview];
	
		//
	[stickerToRemove release];
}

/*********** Deconstruction ***********/

	//
- (void) dealloc
{
		//listen for the XML data to load
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//remove this as a observer of messages
	[nc removeObserver: self];
	
		//
	[self removeGestureRecognizer: tapGesture];
	[self removeGestureRecognizer: rotateGesture];
	[self removeGestureRecognizer: pinchGesture];
	[self removeGestureRecognizer: panGesture];
		
		//remove the stickers
	[self doClearStickers];
	
		//
	[menuView release];
	[pictureContent release];
	[stickerViews release];
	[stickerDrawOverlay release];
	
		//	
    [super dealloc];
}


@end
