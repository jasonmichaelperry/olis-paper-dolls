//
//  OLIControlsOverlayView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>


@interface OLIMainMenuView : UIView
{
		//
	UIImageView *overlayBackgroundView;
	
		//
	UIButton *myDollsButton;	//allows access to the dolls view at any point
	UIButton *myPicturesButton;	//allow access to the pictures view at any point
	UIButton *myStoreButton;	//allow access to the store view at any point
}

	//
- (void) playButtonSelectSound;


@end
