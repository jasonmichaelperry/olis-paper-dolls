//
//  OLIPicturesMenuView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>


@interface OLIPicturesMenuView : UIView
{
		//
	BOOL isEraseMode;
	
		//
	UIButton *clearButton;	//clears all stickers from the stage
	UIButton *dollsButton;	//allows access to the doll stickers
	UIButton *animalsButton;	//allow access to animal stickers
	UIButton *thingsButton;		//allows thing stickers to be shown
	UIButton *buildingsButton;	//allows access to building stickers
	UIButton *shareButton;	//allows you to share the picture
	UIButton *printButton;	//allows printing with the AirPrint framework
}

	//
@property(assign) BOOL isEraseMode;

	//
- (void) playButtonSelectSound;


	//
- (void) setEraseMode: (BOOL) newMode;


@end
