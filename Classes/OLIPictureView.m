//
//  OLIPictureView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPictureView.h"


@implementation OLIPictureView


	//
@synthesize picture;

/*********** Initilization and Construction ***********/

	// 
- (id)initWithFrame: (CGRect)frame
{
		//
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{		
			//maintain a clear background 
		[self setBackgroundColor: [UIColor clearColor]];
		[self setUserInteractionEnabled: YES];
		[self setClipsToBounds: YES];
	}
	
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) setPicture: (OLIPicture *) thePicture
{
		//
	if( picture != nil )
	{	
			//release a reference to anything we already have
		[picture release];		
	}
	
		//get this new doll, and retain it
	picture = [thePicture retain];
	
		//
	[pictureImage removeFromSuperview];
	
		//
	[pictureImage release];
	
		//the image to use for the doll
	UIImage *image = [UIImage imageWithData: [thePicture pictureData]];
	
		//create a view of the image or doll
	pictureImage = [[UIImageView alloc] initWithImage: image];
	
		//
	[pictureImage setFrame: CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
	
		//add it to the view
	[self addSubview: pictureImage];	
}

/*********** Deconstruction ***********/

	//destroies assets
- (void) dealloc
{
		//
	[picture release];
	[pictureImage release];
	
		//
    [super dealloc];
}

@end
