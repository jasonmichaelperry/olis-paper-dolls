//
//  OliPaperDollsAppDelegate.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIDataService.h"

@class OLIPaperDollsViewController;

@interface OLIPaperDollsAppDelegate : NSObject <UIApplicationDelegate>
{
		//
    UIWindow *window;
    OLIPaperDollsViewController *viewController;
	UIImageView *introBackgroundView;	//the background of the intro view
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet OLIPaperDollsViewController *viewController;

@end

