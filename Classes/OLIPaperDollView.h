//
//  OLIPaperDollView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "OLIPaperDoll.h"
#import "OLIOutfit.h"
#import "OLIOutfitView.h"

@interface OLIPaperDollView : UIView
{
		//
	BOOL isInit;
	
		//visual assets
	UIImageView *dollImage;
	
		//
	OLIPaperDoll *doll;
	
		//
	OLIOutfitView *shoesView;
	OLIOutfitView *dressView;
	OLIOutfitView *pantsView;
	OLIOutfitView *shirtView;
	OLIOutfitView *handbagView;
	OLIOutfitView *hatView;
	
		//
	UITapGestureRecognizer *tapGesture;
}

	//
@property( nonatomic, retain ) OLIPaperDoll *doll;

	//
- (void) doOutfitRemove: (UITapGestureRecognizer *) sender;

	//
- (void) setDoll: (OLIPaperDoll *) theDoll;

	//
- (void) addOutfit: (OLIOutfit *) theOutfit;

	//
- (void) addDress: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height;
- (void) addShirt: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height;
- (void) addPants: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height;
- (void) addHat: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height;
- (void) addHandbag: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height;
- (void) addShoes: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height;

	//
- (void) removeOutfit: (OLIOutfit *) theOutfit;

	//
- (void) playAddOutfitSound;

	//
- (void) animateAdd: (OLIOutfitView *) outfitView;
- (void) animateRemove: (OLIOutfitView *) outfitView;
- (void) animateRemoveComplete: (OLIOutfitView *) outfitView;


@end
