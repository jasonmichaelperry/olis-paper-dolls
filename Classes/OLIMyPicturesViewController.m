    //
//  OLIMyPicturesViewController.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIMyPicturesViewController.h"


@implementation OLIMyPicturesViewController

/*********** Initiiliation and Construction ***********/

	//init the sprite on creation
- (id) init
{
		//
	self = [super init];
	
		//
	if( self )
	{
			//listen for the XML data to load
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//register selector to observe for notification
		[nc addObserver:self selector: @selector(doShowPicture:) name:@"OLISelectedPicture" object:nil];
				
			//
		selectedPicture = nil;
				
			//
		mainContent = [[UIView alloc] initWithFrame: CGRectMake( 0, 0, 1024, 768)];
	}
	
		//
	return self;
	
}
	// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{	
		//
	self.view = mainContent;	
}

	// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
		//
    [super viewDidLoad];
}

/*********** Helper Delegates ***********/

	//
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Overriden to allow any orientation.
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

	//
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

/*********** Button and Notification Targets ***********/

	//
- (void) doShowPicture: (NSNotification *) note
{
		//
		//if( pictureArray != nil )
		//[pictureArray release];
	
		//
	if( stickerDictionary != nil )
		[stickerDictionary release];
	
		//
	stickerDictionary = [[[OLIDataService instance] loadStickers] retain];
		
		//
	NSArray *dollStickers = [self dollsToStickers: [[[OLIDataService instance] loadPaperDolls] retain]];
	
		//
	[stickerDictionary setObject: dollStickers forKey: @"Dolls"];
	
		//get the doll to display
	selectedPicture = [[note object] retain];
	
		//create a dressup view
	pictureDesigner = [[OLIPictureDesigner alloc] initWithFrame: CGRectMake( 0, 768, 1024, 768)];
	
		//populate it with the doll and its outfits
	[pictureDesigner displayWithPicture: selectedPicture andStickers: stickerDictionary];
	
		//
	[pictureDesigner setParentController: self];
	
		//
	[pictureSelectorView setUserInteractionEnabled: NO];
	
		//
	[pictureDesigner setUserInteractionEnabled: NO];
	
		//add the view to the stage
	[mainContent addSubview: pictureDesigner];		
	
		//animate
	[UIView beginAnimations: @"showSelectedPictureAnimation" context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowPictureComplete:)];
	
		//point to animate too
	[pictureSelectorView setTransform: CGAffineTransformMakeTranslation(0, -768)];
	[pictureDesigner setTransform: CGAffineTransformMakeTranslation(0, -768)];
	
		//do the animation
	[UIView commitAnimations];
}

	//
- (void) doShowPictureComplete: (NSString *) animationID
{
		//
	[pictureDesigner setUserInteractionEnabled: YES];
	
		//
	[pictureSelectorView removeFromSuperview];
	
		//
	[pictureSelectorView release];
	
		//
	pictureSelectorView = nil;
}

	//
- (void) doShowAllPictures
{
		//
	if( pictureSelectorView != nil )
		return;
	
		//
	if( dollsArray != nil )
		[dollsArray release];
	
		//
	if( pictureArray != nil )
		[pictureArray release];
	
		//
	if( stickerDictionary != nil )
		[stickerDictionary release];
	
		// 
	pictureArray = [[[OLIDataService instance] loadBackgrounds] retain];
	
		//create the select a doll view
	pictureSelectorView = [[OLIPictureScrollView alloc] initWithFrame: CGRectMake( 0, -768, 1024, 768)];
	
		//
	[pictureSelectorView displayPicturesWithData: pictureArray];
	
		//
	[pictureSelectorView setParentController: self];
	
		//
	[pictureSelectorView setUserInteractionEnabled: NO];
	
		//
	if( pictureDesigner != nil )
		[pictureDesigner setUserInteractionEnabled: NO];
	
	
		//set this as the main view
	[mainContent addSubview: pictureSelectorView];
	
		//animate
	[UIView beginAnimations: @"showAllPicturessAnimation" context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowAllPicturesComplete:)];
	
		//point to animate too
	[pictureSelectorView setTransform: CGAffineTransformMakeTranslation(0, 768)];
	
	if( pictureDesigner != nil )
	   [pictureDesigner setTransform: CGAffineTransformMakeTranslation(0, 768)];
	
		//do the animation
	[UIView commitAnimations];
}

	//
- (void) doShowAllPicturesComplete: (NSString *) animationID
{
		//
	[pictureSelectorView setUserInteractionEnabled: YES];
	
		//
	if( pictureDesigner != nil )
	{
			//
		[pictureDesigner removeFromSuperview];
		
			//
		[pictureDesigner release];
		
			//
		pictureDesigner = nil;
	}
}

/*********** Data Populating ***********/

	//
- (void) populate
{
		//
	[self doShowAllPictures];
}

	// 
- (NSArray *) dollsToStickers: (NSArray *) theDolls
{
		//
	NSMutableArray *paperDollStickers = [[NSMutableArray alloc] init];
	 
		//
	for ( OLIPaperDoll *paperDoll in theDolls )
	{
			//
		OLIPaperDollView *ov = [[OLIPaperDollView alloc] initWithFrame: CGRectMake(0, 0, 295, 600)];
		[ov setDoll: paperDoll];
		
			//
		UIGraphicsBeginImageContext(ov.bounds.size);
		[ov.layer renderInContext:UIGraphicsGetCurrentContext()];
		
			//
		UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
			//
		OLISticker *sticker = [[OLISticker alloc] init];
	 
			//set the properties as if is a sticker
		[sticker setStickerId: -[paperDoll dollId]];
		[sticker setStickerName: [paperDoll dollName]];
		[sticker setStickerData: UIImagePNGRepresentation(viewImage)];
		[sticker setStickerWidth: 246];
		[sticker setStickerHeight: 500];
	 
			//
		[paperDollStickers addObject: sticker];
		
			//
		[sticker release];
		
			//
		[ov release];
	}

		//
	[theDolls release];
	
		//
	return [paperDollStickers autorelease];
}

/*********** Animations ***********/

	//
- (void) animateClose
{
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowAllPicturesComplete:)];
	
		//point to animate too
	[self.view setTransform: CGAffineTransformMakeTranslation(0, 768)];
		
		//do the animation
	[UIView commitAnimations];
}


/*********** Deconstruction ***********/

	//
- (void)viewDidUnload
{
		//
    [super viewDidUnload];
	

}

	//
- (void)dealloc
{
		//listen for the XML data to load
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//remove this as a observer of messages
	[nc removeObserver: self];
	
		//
	if( pictureSelectorView != nil )
		[pictureSelectorView release];	
	
		//
	if( pictureDesigner != nil )
		[pictureDesigner release];	
	
		//
	[mainContent release];
	
		//
	[selectedPicture release];
	[stickerDictionary release];
	[pictureArray release];
	[dollsArray release];
		
		//	
    [super dealloc];
}


@end
