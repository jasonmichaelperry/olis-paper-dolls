//
//  OLIPaperDollView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIOutfit.h"

@interface OLIOutfitView : UIView
{
		//visual assets
	UIImageView *outfitImage;
	
		//
	OLIOutfit *outfit;
}

	//
@property( nonatomic, retain ) OLIOutfit *outfit;

	//
- (void) setOutfit: (OLIOutfit *) theOutfit;

@end
