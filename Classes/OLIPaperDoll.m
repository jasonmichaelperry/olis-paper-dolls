//
//  OLIPaperDoll.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPaperDoll.h"


@implementation OLIPaperDoll

	//
@synthesize dollId;
@synthesize dollName;
@synthesize dollData;

	//
@synthesize shoes;
@synthesize dress;
@synthesize pants;
@synthesize shirt;
@synthesize handbag;
@synthesize hat;

	//init the sprite on creation
- (id) init 
{
		//
	self = [super init];
	
		//
	if( self )
	{
			//
		shoes = nil;
		dress = nil;
		pants = nil;
		shirt = nil;
		handbag = nil;
		hat = nil;
	}
	
		//
	return self;
	
}


@end
