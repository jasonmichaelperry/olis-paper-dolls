//
//  OLIShopPacksViewController.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/26/11.
//  Copyright 2011 Jason Michael Perry/Smart Ameba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface OLIShopPacksViewController : UIViewController <SKProductsRequestDelegate>
{

}
	//
- (void) animateClose;

@end
