//
//  OLIStickerView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIStickerView.h"


@implementation OLIStickerView

	//
@synthesize sticker;

/*********** Initilization and Construction ***********/

	//
- (id)initWithFrame:(CGRect)frame
{
		//    
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
		[self setUserInteractionEnabled: YES];
    }
	
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) setSticker: (OLISticker *) theSticker
{
		//release a reference to anything we already have
	[sticker release];
	
		//get this new doll, and retain it
	sticker = [theSticker retain];
	
		//
	[stickerImage removeFromSuperview];
	
		//  
	[stickerImage release];
	
		//the image to use for the doll
	UIImage *image = [UIImage imageWithData: [theSticker stickerData]];
	
		//create a view of the image or doll
	stickerImage = [[UIImageView alloc] initWithImage: image];
	
		//
	[stickerImage setFrame: CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
	
	
		//add it to the view
	[self addSubview: stickerImage];
	
}

/*********** Deconstruction ***********/
	//
- (void)dealloc
{
		//
	[sticker release];
	[stickerImage release];
		
		//
    [super dealloc];
}


@end
