//
//  OLIShopPack.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/26/11.
//  Copyright 2011 Jason Michael Perry/Smart Ameba. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OLIShopPack : NSObject
{
	NSString *packSKU;
	NSString *packName;
	NSString *packDownloadURL;
	NSString *packTeaserImageURL;
}

@property( nonatomic, retain ) NSString *packSKU;
@property( nonatomic, retain ) NSString *packName;
@property( nonatomic, retain ) NSData *packDownloadURL;
@property( nonatomic, retain ) NSData *packTeaserImageURL;

@end
