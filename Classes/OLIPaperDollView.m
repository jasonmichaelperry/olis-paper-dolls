//
//  OLIPaperDollView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPaperDollView.h"


@implementation OLIPaperDollView

	//
@synthesize doll;

/*********** Initilization and Construction ***********/

	//
- (id)initWithFrame: (CGRect)frame
{
		//
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{
			//
		isInit = YES;
		
			//creates a gesture to select a doll
		tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(doOutfitRemove:)];
		tapGesture.numberOfTouchesRequired = 1;	//number of taps needed for a touch

			//add all gestures
		[self addGestureRecognizer: tapGesture];
		
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
		
			//
		shoesView = nil;
		dressView = nil;
		pantsView = nil;
		shirtView = nil;
		hatView = nil;
		handbagView = nil;
   }
	
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) setDoll: (OLIPaperDoll *) theDoll
{
		//release a reference to anything we already have
	[doll release];
	
		//get this new doll, and retain it
	doll = [theDoll retain];
	
		//
	[dollImage removeFromSuperview];
	
		//
	[dollImage release];
	
		//the image to use for the doll
	UIImage *image = [UIImage imageWithData: [doll dollData]];

		//create a view of the image or doll
	dollImage = [[UIImageView alloc] initWithImage: image];
	
		//
	[dollImage setFrame: CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
		
		//add it to the view
	[self addSubview: dollImage];
	
		//do we have shoes?
	if( [doll shoes] != nil )
		[self addOutfit: [doll shoes]];	
	
		//do we have a dress
	if( [doll dress] != nil )
		[self addOutfit: [doll dress]];
	
		//do we have a pants
	if( [doll pants] != nil )
		[self addOutfit: [doll pants]];	
	
		//do we have a shirt
	if( [doll shirt] != nil )
		[self addOutfit: [doll shirt]];
		
		//do we have a hat
	if( [doll hat] != nil )
		[self addOutfit: [doll hat]];
	
		//do we have a handbag
	if( [doll handbag] != nil )
		[self addOutfit: [doll handbag]];
	
		//
	isInit = NO;
	
}


/*********** Add Outfit ***********/

	//
- (void) addOutfit: (OLIOutfit *) theOutfit
{
		//get the outfit being added
	OLIOutfit *outfit = [theOutfit retain];
	
		//location to place the outfit and its size
	float x = 0;
	float y = 40;
	float width = [outfit outfitWidth];
	float height = [outfit outfitHeight];
	
		//we want to handle it based on the type
	if( [[outfit outfitType] isEqualToString: @"Dresses"] && [theOutfit outfitId] != [[dressView outfit] outfitId] )
	{
			//set the proper location
		x = 0;
		y = 100;
		
			///add the dress
		[self addDress: theOutfit atX: x y: y withWidth: width andHeight: height];
	}

		//we want to handle it based on the type
	if( [[outfit outfitType] isEqualToString: @"Shirts"] && [theOutfit outfitId] != [[shirtView outfit] outfitId] )
	{
			//set the proper location
		x = 0;
		y = 100;
		
			///add the dress
		[self addShirt: theOutfit atX: x y: y withWidth: width andHeight: height];
	}
	
		//we want to handle it based on the type
	if( [[outfit outfitType] isEqualToString: @"Pants"] && [theOutfit outfitId] != [[pantsView outfit] outfitId] )
	{
			//set the proper location
		x = 0;
		y = 200;
		
			///add the dress
		[self addPants: theOutfit atX: x y: y withWidth: width andHeight: height];
	}
	
		//we want to handle it based on the type
	if( [[outfit outfitType] isEqualToString: @"Handbags"] && [theOutfit outfitId] != [[handbagView outfit] outfitId] )
	{
			//
		x = 0;
		y = 300;
		
			//
			///add the dress
		[self addHandbag: theOutfit atX: x y: y withWidth: width andHeight: height];
	}

		//we want to handle it based on the type
	if( [[outfit outfitType] isEqualToString: @"Hats"] && [theOutfit outfitId] != [[hatView outfit] outfitId] )
	{
			//
		x = 0;
		y = 0;
		
			//
			///add the dress
		[self addHat: theOutfit atX: x y: y withWidth: width andHeight: height];
	}

		//we want to handle it based on the type
	if( [[outfit outfitType] isEqualToString: @"Shoes"] && [theOutfit outfitId] != [[shoesView outfit] outfitId] )
	{
			//
		x = 0;
		y = 490;
		
			//
			///add the dress
		[self addShoes: theOutfit atX: x y: y withWidth: width andHeight: height];
	}
}

	//
- (void) addDress: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height
{
		//check to see if we have something
	if( [doll dress] != nil  || [dressView outfit] != nil )
	{
			//remove the dress
		[dressView removeFromSuperview];
		
			//release it to
		[dressView release];
	}
	
		//you can not wear both
	if( shirtView != nil )
		[self removeOutfit: [shirtView outfit]];
	
	if( pantsView != nil )
		[self removeOutfit: [pantsView outfit]];
		
		//store the new dress
	[doll setDress: theOutfit];
		
		//
	dressView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( x, y, width, height)];
		
		//store the data to use for the view
	[dressView setOutfit: theOutfit];
		
		//
	if( shoesView != nil )
		[self insertSubview: dressView aboveSubview: shoesView];	
	else
		[self insertSubview: dressView aboveSubview: dollImage];
	
		//
	[self animateAdd: dressView];
}

	//
- (void) addShirt: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height
{
		//check to see if we have something
	if( [doll shirt] != nil  || [shirtView outfit] != nil )
	{
			//remove the dress
		[shirtView removeFromSuperview];
		
			//release it to
		[shirtView release];
	}
	
		//can't wear both
	if( dressView != nil )
		[self removeOutfit: [dressView outfit]];
	
		//store the new dress
	[doll setShirt: theOutfit];
	
		//
	shirtView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( x, y, width, height)];
	
		//store the data to use for the view
	[shirtView setOutfit: theOutfit];
		
		//
	if( pantsView != nil )
		[self insertSubview: shirtView aboveSubview: pantsView];	
	else
		[self addSubview: shirtView];

		//
	[self animateAdd: shirtView];
}

	//
- (void) addPants: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height
{
		//check to see if we have something
	if( [doll pants] != nil  || [pantsView outfit] != nil )
	{
			//remove the dress
		[pantsView removeFromSuperview];
		
			//release it to
		[pantsView release];
	}
	
		//can't wear both
	if( dressView != nil )
		[self removeOutfit: [dressView outfit]];
	
		//store the new dress
	[doll setPants: theOutfit];
	
		//
	pantsView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( x, y, width, height)];
	
		//store the data to use for the view
	[pantsView setOutfit: theOutfit];
	
		//
	if( shoesView != nil )
		[self insertSubview: pantsView aboveSubview: shoesView];	
	else if( shirtView != nil )
		[self insertSubview: pantsView belowSubview: shirtView];
	else
		[self addSubview: pantsView];
	
		//
	[self animateAdd: pantsView];
}

	//
- (void) addHandbag: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height
{
		//check to see if we have something
	if( [doll handbag] != nil || [handbagView outfit] != nil )
	{
			//remove the dress
		[handbagView removeFromSuperview];
		
			//release it to
		[handbagView release];
	}
	
		//store the new dress
	[doll setHandbag: theOutfit];
	
		//
	handbagView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( x, y, width, height)];
	
		//store the data to use for the view
	[handbagView setOutfit: theOutfit];
	
		//
	[self addSubview: handbagView];
	
		//
	[self animateAdd: handbagView];
}

	//
- (void) addHat: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height
{
		//check to see if we have something
	if( [doll hat] != nil || [hatView outfit] != nil )
	{
			//remove the dress
		[hatView removeFromSuperview];
		
			//release it to
		[hatView release];
	}
	
		//store the new dress
	[doll setHat: theOutfit];
	
		//
	hatView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( x, y, width, height)];
	
		//store the data to use for the view
	[hatView setOutfit: theOutfit];
	
		//
	[self addSubview: hatView];
	
		//
	[self animateAdd: hatView];
}

	//
- (void) addShoes: (OLIOutfit *) theOutfit atX: (float) x y: (int) y withWidth: (float) width andHeight: (float) height
{
		//check to see if we have something
	if( [doll shoes] != nil || [shoesView outfit] != nil )
	{
			//remove the dress
		[shoesView removeFromSuperview];
		
			//release it to
		[shoesView release];
	}
	
		//store the new dress
	[doll setShoes: theOutfit];
	
		//
	shoesView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( x, y, width, height)];
	
		//store the data to use for the view
	[shoesView setOutfit: theOutfit];
	
		//
	[self insertSubview: shoesView aboveSubview: dollImage];
	
		//
	[self animateAdd: shoesView];
}

/*********** Outfit Removal ***********/

	//
- (void) removeOutfit: (OLIOutfit *) theOutfit
{	
		//item to remove
	OLIOutfitView *outfitToRemove = nil;
	
		//we want to handle it based on the type
	if( [[theOutfit outfitType] isEqualToString: @"Dresses"] )
	{
			//
		outfitToRemove = dressView;
		
			//
		dressView = nil;

			//
		[doll setDress: nil];
	}

		//we want to handle it based on the type
	else if( [[theOutfit outfitType] isEqualToString: @"Shirts"] )
	{
			//
		outfitToRemove = shirtView;

			//
		shirtView = nil;
		
			//
		[doll setShirt: nil];
	}

		//we want to handle it based on the type
	else if( [[theOutfit outfitType] isEqualToString: @"Pants"] )
	{
			//
		outfitToRemove = pantsView;
		
			//
		pantsView = nil;
		
			//
		[doll setPants: nil];
	}	
	
		//we want to handle it based on the type
	else if( [[theOutfit outfitType] isEqualToString: @"Handbags"] )
	{
			//
		outfitToRemove = handbagView;
		
			//
		handbagView = nil;
		
			//
		[doll setHandbag: nil];
	}
	
		//we want to handle it based on the type
	else if( [[theOutfit outfitType] isEqualToString: @"Hats"] )
	{
			//
		outfitToRemove = hatView;
		
			//
		hatView = nil;
		
			//
		[doll setHat: nil];
	}

		//we want to handle it based on the type
	else if( [[theOutfit outfitType] isEqualToString: @"Shoes"] )
	{
			//
		outfitToRemove = shoesView;
		
			//
		shoesView = nil;
		
			//
		[doll setShoes: nil];
	}
	
		//animate the removal
	if( outfitToRemove != nil )
	{
		[self animateRemove: outfitToRemove];
	}
}

/*********** Sounds ***********/

	//
- (void) playAddOutfitSound
{
		//
	if( isInit == YES )
		return;
	
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"xiao_xia-matt_mat-2290" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);		
}

/*********** Animations ***********/


	//
- (void) animateAdd: (OLIOutfitView *) outfitView
{
		//
	if( isInit == YES )
		return;	
	
		//
	[outfitView setAlpha: 0];
	
		//
	[self playAddOutfitSound];
	
		//
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .20];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionCurlUp];	

		//
	[outfitView setAlpha: 1];
	
		//
	[UIView commitAnimations];
}

	//
- (void) animateRemove: (OLIOutfitView *) outfitView
{
		//
	if( isInit == YES )
		return;	
	
		//
	[outfitView setAlpha: 1];
	
		//
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .20];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationTransitionCurlUp];	
	
		//
	[outfitView setAlpha: 0];
	
		//
	[UIView commitAnimations];
	
		//
    [self performSelector: @selector(animateRemoveComplete:)
			   withObject: outfitView
			   afterDelay: .3
	 ];
	
}

	//
- (void) animateRemoveComplete: (OLIOutfitView *) outfitView
{
		//retreives the default notification center 
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got changed
	[nc postNotificationName: @"OLISaveDoll" object: [self doll]];	
	
		//
	[outfitView removeFromSuperview];
	
		//
	[outfitView release];
	
		//
	outfitView = nil;
}


/*********** Gesture Recognizer ***********/

	//
- (void) doOutfitRemove: (UITapGestureRecognizer *) sender
{		
		//stores the selected outfit
	OLIOutfitView *selectedOutfit = nil;
	
		//originial tap location
	CGPoint tapPoint = [sender locationInView: self];
	
		//the potential outfts being worn
	NSMutableArray *outfitArray = [[NSMutableArray alloc] init];
	
		//handbag view
	if( handbagView != nil )
		[outfitArray addObject: handbagView];

		//hat view
	if( hatView != nil )
		[outfitArray addObject: hatView];

		//hat view
	if( shoesView != nil )
		[outfitArray addObject: shoesView];
	
		//shirts view
	if( shirtView != nil )
		[outfitArray addObject: shirtView];
	
		//pants view
	if( pantsView != nil )
		[outfitArray addObject: pantsView];		
	
		//dress view 
	if( dressView != nil )
		[outfitArray addObject: dressView];	
	
	
		//step through each doll for the selected one
	for( OLIOutfitView *outfitItemView in outfitArray )
	{
			//check to see if the outfit is there or not 
		if( outfitItemView == nil )
			continue;	//hit the next outfit
		
			//used to determine if is in bounds
		CGFloat xMin = outfitItemView.frame.origin.x;
		CGFloat xMax = outfitItemView.frame.origin.x + outfitItemView.frame.size.width;
		CGFloat yMin = outfitItemView.frame.origin.y;
		CGFloat yMax = outfitItemView.frame.origin.y + outfitItemView.frame.size.height;
		
			//it is within
		if( tapPoint.x > xMin && tapPoint.x < xMax && tapPoint.y > yMin && tapPoint.y < yMax )
		{
				//get the paper doll that was selected
			selectedOutfit = outfitItemView;
			
				//no need to keep looping.  We got it.
			break;
		}
	}
	
		//if a paper doll was selected
	if( selectedOutfit != nil )
	{
			//remove it from the user's view
		[self removeOutfit: [selectedOutfit outfit]];
	}
	
		//
		//[selectedOutfit release];
	
		//
	[outfitArray autorelease];
}


/*********** Deconstruction ***********/

	//destroies assets
- (void)dealloc
{
		//
	[self removeGestureRecognizer: tapGesture];
	
		//
	if( dressView != nil )
		[dressView release];

		//
	if( pantsView != nil )
		[pantsView release];

		//
	if( shirtView != nil )
		[shirtView release];
	
		//
	if( handbagView != nil )
		[handbagView release];
		
		//
	if( hatView != nil )
		[hatView release];
	
		//
	if( shoesView != nil )
		[shoesView release];
	
		//
	[doll release];
	[dollImage release];
	
		//
    [super dealloc];
}


@end
