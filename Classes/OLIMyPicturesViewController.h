//
//  OLIMyPicturesViewController.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIDataService.h"
#import "OLIPictureScrollView.h"
#import "OLIPictureDesigner.h"
#import "OLIPicture.h"
#import "OLIPaperDollView.h"
#import "OLIPaperDoll.h"

@interface OLIMyPicturesViewController : UIViewController
{
		//data storage bins
	NSArray *dollsArray;
	NSArray *pictureArray;
	NSMutableDictionary *stickerDictionary;
	
		//
	OLIPictureScrollView *pictureSelectorView;
	OLIPictureDesigner *pictureDesigner;
	
		//the currently selected doll
	OLIPicture *selectedPicture;
	
		//
	UIView *mainContent;
}

	//
- (NSArray *) dollsToStickers: (NSArray *) theDolls;
- (void) doShowAllPictures;
- (void) populate;

	//
- (void) animateClose;

@end
