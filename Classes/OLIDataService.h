//
//  SADataService.h
//  SocialImerssion
//
//  Created by Jason Michael Perry on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
 
#import <Foundation/Foundation.h>
#import "OLIOutfit.h"
#import "OLIPaperDoll.h"
#import "OLIPicture.h"
#import "OLISticker.h"
#import "OLIShopPack.h"
#import "/usr/include/sqlite3.h"

@interface OLIDataService : NSObject
{
	NSString * defaultPath;
}

	//
+ (OLIDataService*) instance;

	//open database call
- (sqlite3 *) openDatabase;
- (void) closeDatabase: (sqlite3 *) database;

	//PaperDolls
- (NSArray *) loadPaperDolls;
- (void) updatePaperDoll: (OLIPaperDoll *) theDoll;
- (void) deletePaperDolOutfits: (int) theID;
	//Outfits
- (NSDictionary *) loadOutfits;
- (NSArray *) loadOutfitsForType: (int) type;
- (OLIOutfit *) loadOutfit: (int) id;

	//Backgrounds
- (NSArray *) loadBackgrounds;

	//Stickers
- (NSDictionary *) loadStickers;
- (NSArray *) loadStickersForType: (int) type;

	//
- (void) insertSticker: (OLISticker *) sticker;
- (void) insertPaperDoll: (OLIPaperDoll *) paperDoll;
- (void) insertBackground: (OLIPicture *) background;
- (void) insertOutfit: (OLIOutfit *) outfit;
- (void) insertStorePack: (OLIShopPack *) shopPack;

@end
