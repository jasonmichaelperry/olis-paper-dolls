//
//  SADataService.m
//  SocialImerssion
//
//  Created by Jason Michael Perry on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIDataService.h"


@implementation OLIDataService

	//
static OLIDataService* _instance = nil;

/*********** Singelton Initilization ***********/

	//
+ (OLIDataService*) instance
{
		//
	@synchronized([OLIDataService class])
	{
			//
		if (!_instance)
		{
				//
			[[self alloc] init];			
		}
		
			//
		return _instance;
	}
	
	return nil;
}

	//
+ (id) alloc
{
		//
	@synchronized([OLIDataService class])
	{
			//
		NSAssert(_instance == nil, @"Attempted to allocate a second instance of a singleton.");
		
			//
		_instance = [super alloc];
		
			//
		return _instance;
	}
	
		//
	return nil;
}

	//Init the Service and DB access
- (id) init
{
		//
	self = [super init];
	
		//
	if( self )
	{
			//
		NSFileManager *fileManager = [NSFileManager defaultManager];

			//
		NSError *error;
		
			//path to the documents directory
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		
			//the location of the default db
		defaultPath = [[[paths lastObject] stringByAppendingPathComponent:@"OLIPaperDolls.sqlite"] retain];
		
			//if its not already there...
		if ( ![fileManager fileExistsAtPath: defaultPath] )
		{
				//
			NSString *shippedPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"OLIPaperDolls.sqlite"];
			
				//
			[fileManager copyItemAtPath: shippedPath toPath: defaultPath error:&error];
		}
	}
	
		//
	return self;
	 
}

/*********** Database Opening and Closing ***********/

	//
- (void) closeDatabase: (sqlite3 *) database
{
		//make sure we shut down the db connection
	sqlite3_close( database );
}

	//
- (sqlite3 *) openDatabase
{
		//
	sqlite3 *database;
		
		//open the database path.  Pass db to as a pointer argument
		//int result = sqlite3_open_v2( [dbPath UTF8String], &database );
	int result = sqlite3_open_v2([defaultPath UTF8String], &database, SQLITE_OPEN_READWRITE, nil );
	
		//did it fail to open?
	if(result != SQLITE_OK)
	{
			//close the database
		sqlite3_close(database);
		
			//create an alert with the error
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Database Error" message: @"Failed to open database." delegate: self cancelButtonTitle: @"Hrm." otherButtonTitles: nil];
		
			//display the alert to the user
		[alertView show];
		[alertView autorelease];
	}
	
		//
	return database;
}

/*********** Paper Doll DB Calls ***********/


	//Loads Paper Dolls
- (NSArray *) loadPaperDolls
{	
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, "SELECT * FROM PaperDoll LEFT OUTER JOIN PaperDollOutfit ON PaperDollOutfit.PaperDollID=PaperDoll.PaperDollID", -1, &statement, nil );
	
		//
	NSMutableArray *paperDollArray = [[NSMutableArray alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
			//
		OLIPaperDoll *paperDoll = [[OLIPaperDoll alloc] init];
		
			//0 should be the ID
		[paperDoll setDollId: sqlite3_column_int( statement, 0 ) ];
		
			//1 should be the name
		[paperDoll setDollName: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)] ];
		
			//2 should be a blob of image data
		[paperDoll setDollData: [NSData dataWithBytes: sqlite3_column_blob( statement, 2) length: sqlite3_column_bytes( statement, 2)] ];
		
			//
		if( SQLITE_NULL != sqlite3_column_type( statement, 5) )
		{
				//
			OLIOutfit* outfit = [self loadOutfit: sqlite3_column_int( statement, 5 )];
			
				//
			[paperDoll setDress: outfit];
			
				//
				//[outfit release];
		}
		
			//
		if( SQLITE_NULL != sqlite3_column_type( statement, 6) )
		{
				//
			OLIOutfit* outfit = [self loadOutfit: sqlite3_column_int( statement, 6 )];
			
				//
			[paperDoll setShirt: outfit];

				//
				//[outfit release];
		}

			//
		if( SQLITE_NULL != sqlite3_column_type( statement, 7) )
		{
				//
			OLIOutfit* outfit = [self loadOutfit: sqlite3_column_int( statement, 7 )];
			
				//
			[paperDoll setPants: outfit];
			
				//
				//[outfit release];
		}

			//
		if( SQLITE_NULL != sqlite3_column_type( statement, 8) )
		{
				//
			OLIOutfit* outfit = [self loadOutfit: sqlite3_column_int( statement, 8 )];
			
				//
			[paperDoll setHat: outfit];
			
				//
				//[outfit release];
		}

			//
		if( SQLITE_NULL != sqlite3_column_type( statement, 9) )
		{
				//
			OLIOutfit* outfit = [self loadOutfit: sqlite3_column_int( statement, 9 )];
			
				//
			[paperDoll setHandbag: outfit];
			
				//
				//[outfit release];
		}

		if( SQLITE_NULL != sqlite3_column_type( statement, 10) )
		{
				//
			OLIOutfit* outfit = [self loadOutfit: sqlite3_column_int( statement, 10 )];
			
				//
			[paperDoll setShoes: outfit];
			
				//
				//[outfit release];
		}		
		
			//
		[paperDollArray addObject: paperDoll];
		
			//
		[paperDoll release];
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [paperDollArray autorelease];
}

	//
- (void) deletePaperDolOutfits: (int) theID
{
		//open database call
	sqlite3 *database = [self openDatabase];
		
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"DELETE FROM PaperDollOutfit WHERE PaperDollID=?";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
	sqlite3_bind_int( statement, 1, theID );
	
		//
	int result = sqlite3_step( statement );
	
		//
	if(result == SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
		NSLog( @"Insert into row id = %d with result: %i", sqlite3_last_insert_rowid(database), result);
	}
	
	else
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
}

	//Save Outfit
- (void) updatePaperDoll: (OLIPaperDoll *) theDoll
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//
	int theID = [theDoll dollId];
	
		//
	[self deletePaperDolOutfits: theID];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"INSERT INTO PaperDollOutfit(PaperDollID,Dress,Shirt,Pants,Hat,Handbag,Shoes) VALUES(?,?,?,?,?,?,?)";
		//NSString *sql = @"INSERT INTO PaperDollOutfit(PaperDollID,Dress,Shirt,Pants,Hat,Handbag) VALUES(1,7,7,7,7,7)";
		//NSString *sql = @"UPDATE PaperDollOutfit SET Dress=?,Shirt=?,Pants=?,Hat=?,Handbag=?, Shoes=? WHERE PaperDollID=?";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
	sqlite3_bind_int( statement, 1, theID );
	
		//
	if( [theDoll dress] != nil )
		sqlite3_bind_int( statement, 2, [[theDoll dress] outfitId] );
	else
		sqlite3_bind_null( statement, 2 );

		//
	if( [theDoll shirt] != nil )
		sqlite3_bind_int( statement, 3, [[theDoll shirt] outfitId] );
	else
		sqlite3_bind_null( statement, 3 );

		//
	if( [theDoll pants] != nil )
		sqlite3_bind_int( statement, 4, [[theDoll pants] outfitId] );
	else
		sqlite3_bind_null( statement, 4 );

		//
	if( [theDoll hat] != nil )
		sqlite3_bind_int( statement, 5, [[theDoll hat] outfitId] );
	else
		sqlite3_bind_null( statement, 5 );

		//
	if( [theDoll handbag] != nil )
		sqlite3_bind_int( statement, 6, [[theDoll handbag] outfitId] );
	else
		sqlite3_bind_null( statement, 6 );

		//
	if( [theDoll shoes] != nil )
		sqlite3_bind_int( statement, 7, [[theDoll shoes] outfitId] );
	else
		sqlite3_bind_null( statement, 7 );

		//
	if( sqlite3_step( statement ) != SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
}

/*********** Outfit DB Calls ***********/


	//Loads all types of Outfits
- (NSDictionary *) loadOutfits
{
		//open database call
	sqlite3 *database = [self openDatabase];	

		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, "SELECT OutfitTypeID, OutfitTypeName FROM OutfitType", -1, &statement, nil );
	
		//
	NSMutableDictionary *outfitDictionaryByType = [[NSMutableDictionary alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
			//
		int outfitTypeID = sqlite3_column_int(statement, 0);
		
			//
		NSString *outfitType = [[NSString alloc] initWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
		
			//
		[outfitDictionaryByType setObject: [self loadOutfitsForType: outfitTypeID] forKey: outfitType];		
		
			//
		[outfitType release];
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [outfitDictionaryByType autorelease];
}

	//Loads Outfit of a Certain Tyoe
- (NSArray *) loadOutfitsForType: (int) type
{
		//open database call
	sqlite3 *database = [self openDatabase];

		//sql statement
	NSString *sql = [NSString stringWithFormat: @"SELECT OutfitItemID, OutfitTypeName, OutfitItemName, OutfitItemSrc, OutfitItemWidth, OutfitItemHeight FROM OutfitItem, OutfitType WHERE OutfitTypeID=OutfitItemType AND OutfitTypeID=%i", type];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, [sql UTF8String], -1, &statement, nil );
	
		//
	NSMutableArray *outfitArray = [[NSMutableArray alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
			//
		OLIOutfit *outfit = [[OLIOutfit alloc] init];
		
			//0 should be the ID
		[outfit setOutfitId: sqlite3_column_int( statement, 0 ) ];
		
			//1 should be the type
		[outfit setOutfitType: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)] ];
		
			//2 should be the name
		[outfit setOutfitName: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 2)] ];
		
			//3 should be a blob of image data
		[outfit setOutfitData: [NSData dataWithBytes: sqlite3_column_blob( statement, 3) length: sqlite3_column_bytes( statement, 3)] ];
		
			//4 should be the width
		[outfit setOutfitWidth: sqlite3_column_double( statement, 4) ];
		
			//5 should be the height
		[outfit setOutfitHeight: sqlite3_column_double( statement, 5) ];
		
			//
		[outfitArray addObject: outfit];
		
			//
		[outfit release];
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [outfitArray autorelease];
}

	//Loads Outfit of a Certain Tyoe
- (OLIOutfit *) loadOutfit: (int) id
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//sql statement
	NSString *sql = [NSString stringWithFormat: @"SELECT OutfitItemID, OutfitTypeName, OutfitItemName, OutfitItemSrc, OutfitItemWidth, OutfitItemHeight FROM OutfitItem, OutfitType WHERE OutfitTypeID=OutfitItemType AND OutfitItemID=%i", id];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, [sql UTF8String], -1, &statement, nil );

		//
	OLIOutfit *outfit = [[OLIOutfit alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
		
			//0 should be the ID
		[outfit setOutfitId: sqlite3_column_int( statement, 0 ) ];
		
			//1 should be the type
		[outfit setOutfitType: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)]];
		
			//2 should be the name
		[outfit setOutfitName: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 2)]];
		
			//3 should be a blob of image data
		[outfit setOutfitData: [NSData dataWithBytes: sqlite3_column_blob( statement, 3) length: sqlite3_column_bytes( statement, 3)] ];
		
			//4 should be the width
		[outfit setOutfitWidth: sqlite3_column_double( statement, 4) ];
		
			//5 should be the height
		[outfit setOutfitHeight: sqlite3_column_double( statement, 5) ];
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );

		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [outfit autorelease];
}

/*********** BG and Pictures DB Calls ***********/

	//Backgrounds
- (NSArray *) loadBackgrounds
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, "SELECT * FROM Backgrounds", -1, &statement, nil );
	
		//
	NSMutableArray *backgroundArray = [[NSMutableArray alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
			//
		OLIPicture *picture = [[OLIPicture alloc] init];
		
			//
		[picture setPictureId: sqlite3_column_int( statement, 0 )];
		
			//
			//[picture setPictureData: [[NSData alloc] initWithBytes: sqlite3_column_blob( statement, 1) length: sqlite3_column_bytes( statement, 1)]];
		[picture setPictureData: [NSData dataWithBytes: sqlite3_column_blob( statement, 1) length: sqlite3_column_bytes( statement, 1)]];
		
			//
		[backgroundArray addObject: picture];
		
			//
		[picture release];
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [backgroundArray autorelease];
}

/*********** Stickers DB Calls ***********/

	//Stickers
- (NSDictionary *) loadStickers
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, "SELECT * FROM StickerType", -1, &statement, nil );
	
		//
	NSMutableDictionary *stickerDictionaryByType = [[NSMutableDictionary alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
			//
		int stickerTypeID = sqlite3_column_int(statement, 0);
		
			//
		NSString *stickerType = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
		
			//
		[stickerDictionaryByType setObject: [self loadStickersForType: stickerTypeID] forKey: stickerType];		
		
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [stickerDictionaryByType autorelease];
}

- (NSArray *) loadStickersForType: (int) type
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//sql statement
	NSString *sql = [NSString stringWithFormat: @"SELECT Sticker.StickerID, StickerType.StickerTypeName, Sticker.StickerName, Sticker.StickerSrc, Sticker.width, Sticker.height FROM Sticker, StickerType WHERE StickerTypeID=Sticker.StickerType AND StickerType.StickerTypeID=%i", type];	

		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//prepare the statement for execution
	sqlite3_prepare_v2( database, [sql UTF8String], -1, &statement, nil );
	
		//
	NSMutableArray *stickerArray = [[NSMutableArray alloc] init];
	
		//step through each result returned
	while( sqlite3_step( statement ) == SQLITE_ROW )
	{
			//
			//
		OLISticker *sticker = [[OLISticker alloc] init];
		
			//0 should be the ID
		[sticker setStickerId: sqlite3_column_int( statement, 0 ) ];
		
			//1 should be the type
		[sticker setStickerType: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)] ];
		
			//2 should be the name
		[sticker setStickerName: [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 2)] ];
		
			//3 should be a blob of image data
		[sticker setStickerData: [NSData dataWithBytes: sqlite3_column_blob( statement, 3) length: sqlite3_column_bytes( statement, 3)] ];

			//4 should be the width
		[sticker setStickerWidth: sqlite3_column_double( statement, 4) ];
		
			//5 should be the height
		[sticker setStickerHeight: sqlite3_column_double( statement, 5) ];
		
			//
		[stickerArray addObject: sticker];
		
			//
		[sticker release];
	}
	
		//
	sqlite3_reset( statement );
	
		//clean up the execution of the call
	sqlite3_finalize( statement );

		//closes the databse call
	[self closeDatabase: database];
	
		//
	return [stickerArray autorelease];
}

	//
- (void) updatePicture
{
	
}

/*********** Store Pack DB Calls ***********/


	//
- (void) insertSticker: (OLISticker *) sticker
{
		//open database call
	sqlite3 *database = [self openDatabase];
				
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"INSERT INTO Stickers(StickerName,StickerType,StickerSrc,width,height) VALUES(?,?,?,?,?)";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
		//sqlite3_bind_double( statement, 1, [sticker stickerWidth] );
		//sqlite3_bind_double( statement, 1, [sticker stickerHeight] );
		
		//
	if( sqlite3_step( statement ) != SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
	
}

	//
- (void) insertPaperDoll: (OLIPaperDoll *) paperDoll
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"INSERT INTO Stickers(StickerName,StickerType,StickerSrc,width,height) VALUES(?,?,?,?,?)";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
		//sqlite3_bind_double( statement, 1, [sticker stickerWidth] );
		//sqlite3_bind_double( statement, 1, [sticker stickerHeight] );
	
		//
	if( sqlite3_step( statement ) != SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];	
}

	//
- (void) insertBackground: (OLIPicture *) background
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"INSERT INTO Stickers(StickerName,StickerType,StickerSrc,width,height) VALUES(?,?,?,?,?)";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
		//sqlite3_bind_double( statement, 1, [sticker stickerWidth] );
		//sqlite3_bind_double( statement, 1, [sticker stickerHeight] );
	
		//
	if( sqlite3_step( statement ) != SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];	
}

	//
- (void) insertOutfit: (OLIOutfit *) outfit
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"INSERT INTO Stickers(StickerName,StickerType,StickerSrc,width,height) VALUES(?,?,?,?,?)";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
		//sqlite3_bind_double( statement, 1, [sticker stickerWidth] );
		//sqlite3_bind_double( statement, 1, [sticker stickerHeight] );
	
		//
	if( sqlite3_step( statement ) != SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];	
}

	//
- (void) insertStorePack: (OLIShopPack *) shopPack
{
		//open database call
	sqlite3 *database = [self openDatabase];
	
		//the sql statement to store results
	sqlite3_stmt *statement;
	
		//sql statement
	NSString *sql = @"INSERT INTO Stickers(StickerName,StickerType,StickerSrc,width,height) VALUES(?,?,?,?,?)";
	
		//prepare the statement for execution
	sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, nil );
	
		//
		//sqlite3_bind_double( statement, 1, [sticker stickerWidth] );
		//sqlite3_bind_double( statement, 1, [sticker stickerHeight] );
	
		//
	if( sqlite3_step( statement ) != SQLITE_DONE )
	{
		NSLog( @"Error: %s", sqlite3_errmsg(database) );
	}
	
		//
	sqlite3_reset( statement );
	
		//
	sqlite3_finalize( statement );
	
		//closes the databse call
	[self closeDatabase: database];
}


/*********** Deconstruction ***********/

	//
- (void) dealloc
{
		//
	[defaultPath release];
	
		//
    [super dealloc];
}

@end
