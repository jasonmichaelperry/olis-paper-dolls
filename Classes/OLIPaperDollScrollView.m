//
//  OLIPaperDollSelectorView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPaperDollScrollView.h"


@implementation OLIPaperDollScrollView

@synthesize parentController;

/*********** Initilization and Construction ***********/

	//
- (id) initWithFrame:(CGRect)frame
{
		//
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{
			//No doll yet selected
		selectedPaperDoll = nil;
		
			//retains space for the doll views
		paperDollViews = [[NSMutableArray alloc] init];
		
			//creates a gesture to select a doll
		tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(doDollSelect:)];
		tapGesture.numberOfTouchesRequired = 1;	//number of taps needed for a touch
											//[tapGesture setDelaysTouchesBegan: YES];
		
			//create a gesture to swipe left though dolls
		panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(doDollPan:)];
		panGesture.maximumNumberOfTouches = 1;
		
			//add all gestures
		[self addGestureRecognizer: tapGesture];
		[self addGestureRecognizer: panGesture];
		
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
				
			//
		dollsView = [[UIView alloc] init];
		
			//
		[self addSubview: dollsView];
    }
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) displayPaperDollsWithData: (NSArray *) dollData
{
		//retains the local NSArray in memory
	paperDollArray = [dollData retain];

		//
	[self displayPaperDollsWithX: 0 andY: (768/2) - (600/2) forWidth: 295 andHeight: 600];
}

	//
- (void) displayPaperDollThumbnailsWithData: (NSArray *) dollData
{
		//retains the local NSArray in memory
	paperDollArray = [dollData retain];
	
		//
	[self displayPaperDollsWithX: 0 andY: 0 forWidth: [self sizeToHeight: 100 from: CGSizeMake( 295, 600)] andHeight: 100];
}

	//
- (void) displayPaperDollsWithX: (float) theX andY: (float) theY forWidth: (float) theWidth andHeight: (float) theHeight
{	
		//step through the array of dolls and show them
	for ( int i = 0; i < [paperDollArray count]; i++ )
	{
			//create a model instance of the doll
		OLIPaperDoll *paperDoll = (OLIPaperDoll *)[paperDollArray objectAtIndex: i];
		
			//create a view for this doll
		OLIPaperDollView *paperDollView = [[OLIPaperDollView alloc] initWithFrame: CGRectMake( theX, theY, theWidth, theHeight)];
		[paperDollView setDoll: paperDoll];

			//populate it into the display
		[dollsView addSubview: paperDollView];
		
			//store the view in the array
		[paperDollViews addObject: paperDollView];
		
			//
		[paperDollView release];
		
			//
		theX += theWidth;
	}
	
		//
	actualWidth = theX;
}

/*********** Gesture Selectors ***********/

	//
- (void) doDollSelect: (UITapGestureRecognizer *) sender
{		
		//originial tap location
	CGPoint tapPoint = [sender locationInView: dollsView];

		//step through each doll for the selected one
	for( OLIPaperDollView *paperDoll in paperDollViews )
	{
			//used to determine if is in bounds
		CGFloat xMin = paperDoll.frame.origin.x;
		CGFloat xMax = paperDoll.frame.origin.x + paperDoll.frame.size.width;
		CGFloat yMin = paperDoll.frame.origin.y;
		CGFloat yMax = paperDoll.frame.size.height;
		
			//it is within
		if( tapPoint.x > xMin && tapPoint.x < xMax && tapPoint.y > yMin && tapPoint.y < yMax )
		{
				//get the paper doll that was selected
			selectedPaperDoll = [paperDoll retain];
			
				//no need to keep looping.  We got it.
			break;
		}
	}
	
		//if a paper doll was selected
	if( selectedPaperDoll != nil )
	{
		[self playDollSelectSound];
		
			//retreives the default notification center
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//posts a notification that show dolls got selected
		[nc postNotificationName: @"OLISelectedDoll" object: [selectedPaperDoll doll]];
	}
}

	// 
- (void) doDollPan: (UIPanGestureRecognizer *) sender
{
		//stores first point from beginning 
	static CGPoint first;
	
		//to store the translation point and final point
	CGPoint transPoint = [sender translationInView: dollsView];
	CGPoint final;
	
		//is this beginning of the pan?
	if( [sender state] == UIGestureRecognizerStateBegan )
	{
			//get the first values
		first = [dollsView center];
	}
	
		//determine the final location
	final = CGPointMake( first.x + transPoint.x, first.y );
	
		//
	if( fabs(final.x) > ( actualWidth - self.frame.size.width ) )
		final.x = -( actualWidth - self.frame.size.width );
	
		//make sure we did not scroll to far
	if( final.x > 0 )
		final.x = 0;	
	
		
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .25];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
		
		//point to animate too
	[dollsView setCenter: CGPointMake( final.x, first.y)];
		
		//do the animation
	[UIView commitAnimations];				
}


/*********** Sounds ***********/

	//
- (void) playDollSelectSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"Clook-Public_D-9" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);		
}


/*********** Helper Methods ***********/

	//
- (float) sizeToHeight: (float) toHeight from: (CGSize) fromSize
{
		//original height / original width x new width = new height
	return (fromSize.width/fromSize.height) * toHeight;
}


	//
- (float) sizeToWidth: (float) theWidth from: (CGSize) fromSize
{
		//original height / original width x new width = new height
	return (fromSize.height/fromSize.width) * theWidth;
}

/*********** Deconstruction ***********/

	//
- (void) dealloc
{
		//
	[self removeGestureRecognizer: tapGesture];
	[self removeGestureRecognizer: panGesture];
	
		//step through each doll for the selected one
	for( OLIPaperDollView *paperDoll in paperDollViews )
	{
			//
		[paperDoll removeFromSuperview];		
	}		
	
		//
	if( selectedPaperDoll != nil )
	{
			//
		[selectedPaperDoll release];		
	}
	
		//
	[paperDollArray release];
	[paperDollViews release];
	
		//
    [super dealloc];
}


@end
