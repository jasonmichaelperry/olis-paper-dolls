//
//  OLIPaperDollView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIOutfitView.h"


@implementation OLIOutfitView

	//
@synthesize outfit;

/*********** Initilization and Construction ***********/

	//
- (id)initWithFrame: (CGRect)frame
{
		//
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{		
			// 
		outfit = nil;
		
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
		[self setUserInteractionEnabled: YES];
   }
	
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) setOutfit: (OLIOutfit *) theOutfit
{
		//release a reference to anything we already have 
	[outfit release];
	
		//get this new doll, and retain it
	outfit = [theOutfit retain];
	
		//
	[outfitImage removeFromSuperview];
	
		//
	[outfitImage release];
	
		//the image to use for the doll
	UIImage *image = [UIImage imageWithData: [theOutfit outfitData]];

		//create a view of the image or doll
	outfitImage = [[UIImageView alloc] initWithImage: image];
	
		//add it to the view
	[self addSubview: outfitImage];
		
}
	
/*********** Deconstruction ***********/

	//destroies assets
- (void)dealloc
{
		//
	[outfit release];
	[outfitImage release];
	
		//
    [super dealloc];
}


@end
