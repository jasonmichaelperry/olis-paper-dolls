//
//  OutfitScrollView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIOutfitView.h"
#import "OLIOutfit.h"

@interface OLIOutfitScrollView : UIView
{
		//
	float actualWidth;
	
		//data storage bins
	NSArray *outfitsArray;
	
		//array of all dolls
	NSMutableArray *outfitViewsArray;
	
		//visual controls
	UIView *outfitsView;	//large view contains all of the dolls
	
		//the currently selected doll
	OLIOutfitView *selectedOutfit;
	
		//
	UITapGestureRecognizer *tapGesture;
	UIPanGestureRecognizer *panGesture;
}

	//
- (void) displayOutfitsWithData: (NSArray *) outfitData offset: (float) startX andSeperation: (float) spacing;

@end
