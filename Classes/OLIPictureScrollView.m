//
//  OLIPictureScrollView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPictureScrollView.h"


@implementation OLIPictureScrollView

@synthesize parentController;

	//
- (id)initWithFrame:(CGRect)frame
{
		//
    self = [super initWithFrame:frame];
	
		//
    if (self)
	{
			//No doll yet selected
		selectedPicture = nil;
		
			//retains space for the doll views
		picturesViewsArray = [[NSMutableArray alloc] init];
		
			//creates a gesture to select a doll
		tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(doPictureSelect:)];
		tapGesture.numberOfTouchesRequired = 1;	//number of taps needed for a touch
		
			//create a gesture to swipe left though dolls
		panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(doPicturePan:)];
		panGesture.maximumNumberOfTouches = 1;
		
			//add all gestures
		[self addGestureRecognizer: tapGesture];
		[self addGestureRecognizer: panGesture];
		
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
		[self setClipsToBounds: YES];
		[self setUserInteractionEnabled: YES];
		
			// 
		picturesView = [[UIView alloc] init];
		
			//
		[self addSubview: picturesView];
		
    }
	
		//
    return self;
}
/*********** Data Population ***********/

	//
- (void) displayPicturesWithData: (NSArray *) pictureData;
{
		//retains the local NSArray in memory
	picturesArray = [pictureData retain];
	
		//
	float xPos = 20;
	float yPos = (768/2)-(500/2);
	float spacer = 20;
	float theWidth = 0;
	float theHeight = 500;
	
		//step through the array of dolls and show them
	for ( int i = 0; i < [picturesArray count]; i++ )
	{
			//create a model instance of the doll
		OLIPicture *picture = (OLIPicture *)[picturesArray objectAtIndex: i];
		
		theWidth =  [self sizeToHeight: theHeight from: CGSizeMake( 1024, 768)];
		
			//create a view for this doll
		OLIPictureView *pictureItemView = [[OLIPictureView alloc] initWithFrame: CGRectMake( xPos, yPos, theWidth, theHeight )];
		[pictureItemView setPicture: picture];
		
			//populate it into the display
		[picturesView addSubview: pictureItemView];
		
			//store the view in the array 
		[picturesViewsArray addObject: pictureItemView];
		
			//
		[pictureItemView release];
		
			//
		xPos += theWidth + spacer;
	}
	
		//
	actualWidth = xPos;
}

/*********** Gesture Selectors ***********/

	//
- (void) doPictureSelect: (UITapGestureRecognizer *) sender
{		
		//originial tap location
	CGPoint tapPoint = [sender locationInView: picturesView];
	
		//step through each doll for the selected one
	for( OLIPictureView *pictureView in picturesViewsArray )
	{
			//used to determine if is in bounds
		CGFloat xMin = pictureView.frame.origin.x;
		CGFloat xMax = pictureView.frame.origin.x + pictureView.frame.size.width;
		CGFloat yMin = pictureView.frame.origin.y;
		CGFloat yMax = pictureView.frame.origin.y + pictureView.frame.size.height;
		
			//it is within
		if( tapPoint.x > xMin && tapPoint.x < xMax && tapPoint.y > yMin && tapPoint.y < yMax )
		{
				//get the paper doll that was selected
			selectedPicture = pictureView;
			
				//no need to keep looping.  We got it.
			break;
		}
	}
	
		//if a paper doll was selected
	if( selectedPicture != nil )
	{
			//
		[self playStickerSelectSound];
		
			//retreives the default notification center
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//posts a notification that show dolls got selected
		[nc postNotificationName: @"OLISelectedPicture" object: [selectedPicture picture]];
	}
}

	//
- (void) doPicturePan: (UIPanGestureRecognizer *) sender
{
		//stores first point from beginning
	static CGPoint first;
	
		//to store the translation point and final point
	CGPoint transPoint = [sender translationInView: picturesView];
	CGPoint final;
	
		//is this beginning of the pan?
	if( [sender state] == UIGestureRecognizerStateBegan )
	{
			//get the first values
		first = [picturesView center];
	}
	
		//determine the final location
	final = CGPointMake( first.x + transPoint.x, first.y );
	
		//
	if( fabs(final.x) > ( actualWidth - self.frame.size.width ) )
		final.x = -( actualWidth - self.frame.size.width );
	
		//make sure we did not scroll to far
	if( final.x > 0 )
		final.x = 0;		
	
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .25];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
	
		//point to animate too
	[picturesView setCenter: CGPointMake( final.x, first.y)];
	
		//do the animation
	[UIView commitAnimations];				
}

/*********** Sounds ***********/

	//
- (void) playStickerSelectSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"Clook-Public_D-9" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);	
}

/*********** Helper Methods ***********/

	//
- (float) sizeToHeight: (float) toHeight from: (CGSize) fromSize
{
		//original height / original width x new width = new height
	return (fromSize.width/fromSize.height) * toHeight;
}

	//
- (float) sizeToWidth: (float) theWidth from: (CGSize) fromSize
{
		//original height / original width x new width = new height
	return (fromSize.height/fromSize.width) * theWidth;
}


/*********** Deconstruction ***********/

- (void)dealloc
{
		//
	[self removeGestureRecognizer: tapGesture];
	[self removeGestureRecognizer: panGesture];
	
		//
		//[tapGesture release];
		//[panGesture release];
	
		//
	[picturesArray release];
	[picturesViewsArray release];
	[picturesView release];
	
		//
    [super dealloc];
}


@end
