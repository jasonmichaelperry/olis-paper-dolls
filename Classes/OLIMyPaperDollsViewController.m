    //
//  OLIMyPaperDollsViewController.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIMyPaperDollsViewController.h"


@implementation OLIMyPaperDollsViewController

/*********** Initiiliation and Construction ***********/

	//init the sprite on creation
- (id) init
{
		//
	self = [super init];
	
		//
	if( self )
	{
			//listen for the XML data to load
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//register selector to observe for notification
		[nc addObserver:self selector: @selector(doShowDoll:) name:@"OLISelectedDoll" object:nil];
		[nc addObserver:self selector:@selector(updatePaperDoll:) name:@"OLISaveDoll" object:nil];		
		
			//
		selectedPaperDoll = nil;
		
			//
		outfitDictionary = nil;
		
			//
		paperDollArray = nil;
				
			//
		mainContent = [[UIView alloc] initWithFrame: CGRectMake( 0, 0, 1024, 768)];
	}

		//
	return self;
	
}

	// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
		//
	self.view = mainContent;
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
 {
    [super viewDidLoad];
}
*/

/*********** Helper Delegates ***********/

	//
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{	
    // Overriden to allow any orientation.
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

	//
- (void) didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

/*********** Button and Notification Targets ***********/

	//
- (void) updatePaperDoll: (NSNotification *) note
{
		//
	[[OLIDataService instance] updatePaperDoll: [note object]];
}

	//
- (void) doShowDoll: (NSNotification *) note
{	
		//
	selectedPaperDoll = [[note object] retain];
	
		//
	if( selectedPaperDoll == nil )
		[selectedPaperDoll release];
	
		//
	if( outfitDictionary == nil )
		[outfitDictionary release];		
	
		//
	outfitDictionary = [[[OLIDataService instance] loadOutfits] retain];		
		
		//create a dressup view
	dressUpView = [[OLIPaperDollDressUpView alloc] initWithFrame: CGRectMake( 0, 768, 1024, 768)];

		//populate it with the doll and its outfits
	[dressUpView displayPaperDoll: selectedPaperDoll withOutfits: outfitDictionary];

		//
	[dressUpView setUserInteractionEnabled: NO];
	
		//
	[paperDollSelectorView setUserInteractionEnabled: NO];
	
		//
	[dressUpView setParentController: self];

	
		//add the view to the stage
	[mainContent insertSubview: dressUpView aboveSubview:paperDollSelectorView];		
		
		//animate
	[UIView beginAnimations: @"showSelectedDollAnimation" context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowDollComplete:)];
	
		//point to animate too
	[paperDollSelectorView setTransform: CGAffineTransformMakeTranslation(0, -768)];
	[dressUpView setTransform: CGAffineTransformMakeTranslation(0, -768)];
	
		//do the animation
	[UIView commitAnimations];
	
		//
	[selectedPaperDoll release]; 
}

	//
- (void) doShowDollComplete: (NSString *) animationID
{
		//
	[dressUpView setUserInteractionEnabled: YES];
	
		//
	[paperDollSelectorView removeFromSuperview];
	
		//
	[paperDollSelectorView release];
	
		//
	paperDollSelectorView = nil;
}

	//
- (void) doShowAllDolls
{
		//
	if( paperDollSelectorView != nil )
		return;
	
		//
	if( paperDollArray != nil )
		[paperDollArray release];
	
		//
	if( outfitDictionary != nil )
		[outfitDictionary release];
	
		//
	paperDollArray = [[[OLIDataService instance] loadPaperDolls] retain];

		//create the select a doll view
	paperDollSelectorView = [[OLIPaperDollScrollView alloc] initWithFrame: CGRectMake( 0, -768, 1024, 768)];	
	
		//give the views somethin' to show
	[paperDollSelectorView displayPaperDollsWithData: paperDollArray];
	
		//
	[paperDollSelectorView setParentController: self];
	
		//
	[paperDollSelectorView setUserInteractionEnabled: NO];
	
		//
	if( dressUpView != nil )
		[dressUpView setUserInteractionEnabled: NO];
	
		//set this as the main view
	[mainContent addSubview: paperDollSelectorView];
	
		//animate
	[UIView beginAnimations: @"showAllDollsAnimation" context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowAllDollsComplete:)];
	
		//point to animate too
	[paperDollSelectorView setTransform: CGAffineTransformMakeTranslation(0, 768)];
	
	if( dressUpView != nil )
		[dressUpView setTransform: CGAffineTransformMakeTranslation(0, 768)];		

		//do the animation
	[UIView commitAnimations];
}

	//
- (void) doShowAllDollsComplete: (NSString *) animationID
{
		//
	[paperDollSelectorView setUserInteractionEnabled: YES];

		//get the doll to display
	if( selectedPaperDoll != nil )
	{
		
			//
		[selectedPaperDoll release];
	}
	
		//
	[dressUpView removeFromSuperview];
		
		//
	[dressUpView release];
	
		//
	dressUpView = nil;
}


/*********** Data Populating ***********/

	// 
- (void) populate
{
		//load data from database
	[self doShowAllDolls];
}

/*********** Animations ***********/

	//
- (void) animateClose
{
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowAllPicturesComplete:)];
	
		//point to animate too
	[self.view setTransform: CGAffineTransformMakeTranslation(0, 768)];
	
		//do the animation
	[UIView commitAnimations];
}

/*********** Deconstruction ***********/

	//
- (void) viewDidUnload
{
		//
    [super viewDidUnload];


}

	//
- (void) dealloc
{
		//listen for the XML data to load
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//remove this as a observer of messages
	[nc removeObserver: self];		
	
		//
	if( paperDollSelectorView != nil )
		[paperDollSelectorView release];		
	
		//
	if( dressUpView != nil )
		[dressUpView release];
	
		//
	[mainContent release];
	
		//
	[outfitDictionary release];
	[paperDollArray release];
		//[selectedPaperDoll release];
	
		//
    [super dealloc];
}


@end
