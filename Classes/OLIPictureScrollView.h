//
//  OLIPictureScrollView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "OLIPicture.h"
#import "OLIPictureView.h"

@interface OLIPictureScrollView : UIView
{
		//
	float actualWidth;
	
		//data storage bins
	NSArray *picturesArray;
	
		//array of all dolls
	NSMutableArray *picturesViewsArray;
	
		//visual controls
	UIView *picturesView;	//large view contains all of the dolls
	
		//the currently selected picture
	OLIPictureView *selectedPicture;
	
		//
	UITapGestureRecognizer *tapGesture;
	UIPanGestureRecognizer *panGesture;
	
		//
    UIViewController *parentController;
}

	// Don't use retain or you'll have a circular reference
@property(nonatomic, assign) UIViewController *parentController;

	//
- (void) displayPicturesWithData: (NSArray *) pictureData;

	//
- (float) sizeToHeight: (float) toHeight from: (CGSize) fromSize;
- (float) sizeToWidth: (float) theWidth from: (CGSize) fromSize;

	//
- (void) playStickerSelectSound;


@end
