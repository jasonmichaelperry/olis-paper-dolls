//
//  OLIMyPaperDollsViewController.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIDataService.h"
#import "OLIPaperDollScrollView.h"
#import "OLIPaperDollDressUpView.h"

@interface OLIMyPaperDollsViewController : UIViewController
{
		//data storage bins
	NSArray *paperDollArray;
	NSDictionary *outfitDictionary;
	
		//
	OLIPaperDollScrollView *paperDollSelectorView;
	OLIPaperDollDressUpView *dressUpView;
	
		//the currently selected doll
	OLIPaperDoll *selectedPaperDoll;
	
		//
	UIView *mainContent;
}

	//
- (void) doShowDoll: (NSNotification *) note;
- (void) doShowAllDolls;
- (void) populate;

	//
- (void) animateClose;

@end
