//
//  OLIOutfit.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OLIOutfit : NSObject
{
	int outfitId;
	NSString *outfitName;
	NSString *outfitType;
	NSData *outfitData;
	float outfitWidth;
	float outfitHeight;
}

@property( assign ) int outfitId;
@property( nonatomic, retain ) NSString *outfitName;
@property( nonatomic, retain ) NSString *outfitType;
@property( nonatomic, retain ) NSData *outfitData;
@property( assign ) float outfitWidth;
@property( assign ) float outfitHeight;

@end
