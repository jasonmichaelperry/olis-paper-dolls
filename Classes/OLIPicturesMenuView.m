//
//  OLIPicturesMenuView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPicturesMenuView.h"


@implementation OLIPicturesMenuView

@synthesize isEraseMode;

/*********** Initilization and Construction ***********/

	//
- (id)initWithFrame:(CGRect)frame
{
		//    
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{
			//
		isEraseMode = NO;
		
			//create the button for pictures
		clearButton = [[UIButton alloc] initWithFrame: CGRectMake( 10, 0, 100, 100 )];
		
			//
		[clearButton addTarget: self action: @selector(doClearStickers:) forControlEvents: UIControlEventTouchUpInside];
		[clearButton setImage: [UIImage imageNamed: @"iconreraser1"] forState: UIControlStateNormal];
		
			//create the button to tap to myDolls
		dollsButton = [[UIButton alloc] initWithFrame: CGRectMake( 120, 0, 100, 100 )];
		
			//
		[dollsButton addTarget: self action: @selector(doShowStickers:) forControlEvents: UIControlEventTouchUpInside];
		[dollsButton setImage: [UIImage imageNamed: @"adddolls"] forState: UIControlStateNormal];
		[dollsButton setTitle: @"Dolls" forState: UIControlStateNormal];
		[dollsButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];
		
			//create the button for pictures
		animalsButton = [[UIButton alloc] initWithFrame: CGRectMake( 220, 0, 100, 100 )];
		
			//
		[animalsButton addTarget: self action: @selector(doShowStickers:) forControlEvents: UIControlEventTouchUpInside];
		[animalsButton setImage: [UIImage imageNamed: @"addanimals"] forState: UIControlStateNormal];
		[animalsButton setTitle: @"Animals" forState: UIControlStateNormal];
		[animalsButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];

			//create the button for pictures
		thingsButton = [[UIButton alloc] initWithFrame: CGRectMake( 320, 0, 100, 100 )];
		
			//
		[thingsButton addTarget: self action: @selector(doShowStickers:) forControlEvents: UIControlEventTouchUpInside];
		[thingsButton setImage: [UIImage imageNamed: @"addthings"] forState: UIControlStateNormal];
		[thingsButton setTitle: @"Things" forState: UIControlStateNormal];
		[thingsButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];
		
			//create the button for pictures
		buildingsButton = [[UIButton alloc] initWithFrame: CGRectMake( 420, 0, 100, 100 )];
		
			//
		[buildingsButton addTarget: self action: @selector(doShowStickers:) forControlEvents: UIControlEventTouchUpInside];
		[buildingsButton setImage: [UIImage imageNamed: @"addfurniture"] forState: UIControlStateNormal];
		[buildingsButton setTitle: @"Buildings" forState: UIControlStateNormal];
		[buildingsButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];
		
			//create the button for pictures
		shareButton = [[UIButton alloc] initWithFrame: CGRectMake( 520, 0, 100, 100 )];
		
			//
		[shareButton addTarget: self action: @selector(doShare:) forControlEvents: UIControlEventTouchUpInside];
		[shareButton setImage: [UIImage imageNamed: @"email"] forState: UIControlStateNormal];
		[shareButton setTitle: @"Share" forState: UIControlStateNormal];
		[shareButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];
		
			//create the button for pictures
		printButton = [[UIButton alloc] initWithFrame: CGRectMake( 620, 0, 100, 100 )];
		
			//
		[printButton addTarget: self action: @selector(doPrint:) forControlEvents: UIControlEventTouchUpInside];
		[printButton setImage: [UIImage imageNamed: @"print"] forState: UIControlStateNormal];
		[printButton setTitle: @"Print" forState: UIControlStateNormal];
		[printButton setTitleColor: [UIColor clearColor] forState: UIControlStateNormal];
			
			//
		[self setBackgroundColor: [UIColor clearColor]];
		
			//add assets to the stage
		[self addSubview: clearButton];
		[self addSubview: dollsButton];	
		[self addSubview: animalsButton];
		[self addSubview: thingsButton];
		[self addSubview: buildingsButton];	
		[self addSubview: shareButton];
		[self addSubview: printButton];	
    }
	
		//
    return self;
}

/*********** Button Selectors ***********/

	//
- (void) doClearStickers: (UIButton *) target
{
		//
	[self playButtonSelectSound];
	
		//
	[self setEraseMode: YES];	
}

	//
- (void) doShowStickers: (UIButton *) target
{	
		//do not erase
	[self setEraseMode: NO];
	
		//
	[self playButtonSelectSound];
	
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got selected
	[nc postNotificationName: @"OLIShowStickers" object: target];
}

	//
- (void) doShare: (UIButton *) target
{
		//do not erase
	[self setEraseMode: NO];
	
		//
	[self playButtonSelectSound];
	
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got selected
	[nc postNotificationName: @"OLIPictureShare" object: nil];
}

	//
- (void) doPrint: (UIButton *) target
{
		//do not erase
	[self setEraseMode: NO];
	
		//
	[self playButtonSelectSound];

	
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got selected
	[nc postNotificationName: @"OLIPicturePrint" object: target];
}

- (void) setEraseMode: (BOOL) newMode
{
	isEraseMode = newMode;
	
	if( isEraseMode == YES )
		[clearButton setAlpha: .8];
	
	else
		[clearButton setAlpha: 1];
	
}


/*********** Sounds ***********/

	// 
- (void) playButtonSelectSound
{
		//Get the filename of the sound file:
	NSString *soundPath = [[NSBundle mainBundle] pathForResource: @"Clook-Public_D-9" ofType: @"wav"];
	
		//declare a system sound id
	SystemSoundID soundID;
	
		//Get a URL for the sound file
	NSURL *soundFilePath = [NSURL fileURLWithPath:soundPath isDirectory:NO];
	
		//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((CFURLRef)soundFilePath, &soundID);
	
		//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);		
}

/*********** Deconstruction ***********/

- (void)dealloc
{
		//
	[clearButton release];
	[dollsButton release];
	[animalsButton release];
	[thingsButton release];
	[buildingsButton release];
	[shareButton release];
	[printButton release];

		//
    [super dealloc];
}


@end
