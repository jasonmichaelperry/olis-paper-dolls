//
//  OLIPaperDollDressUpView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIPaperDoll.h"
#import "OLIPaperDollView.h"
#import "OLIOutfit.h"
#import "OLIOutfitView.h"
#import "OLIOutfitScrollView.h"

@interface OLIPaperDollDressUpView : UIView
{
		//stores all of the data
	NSDictionary *outfits;
	
		//
	UIImageView *closetView;
	
		//the paper doll dress up
	OLIPaperDollView *paperDoll;
		
		//views for each grouping of outfits
	OLIOutfitScrollView *shoesScrolView;
	OLIOutfitScrollView *hatScrolView;
	OLIOutfitScrollView *handbagScrolView;
	OLIOutfitScrollView *dressesScrolView;	
	OLIOutfitScrollView *shirtsScrolView;	
	OLIOutfitScrollView *pantsScrolView;	

		//
    UIViewController *parentController;
}

	// Don't use retain or you'll have a circular reference
@property(nonatomic, assign) UIViewController *parentController;

	//
- (void) displayPaperDoll: (OLIPaperDoll *) thePaperDoll withOutfits: (NSDictionary *) theOutfits;

	//
- (void) doAddOutfit: (NSNotification *) note;

	//
- (void) savePaperDoll;

@end
