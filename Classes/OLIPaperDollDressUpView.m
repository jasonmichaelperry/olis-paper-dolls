//
//  OLIPaperDollDressUpView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPaperDollDressUpView.h"


@implementation OLIPaperDollDressUpView

@synthesize parentController;

/*********** Initilization and Construction ***********/

	//object on stage should be editable when added
	//and on second delete does

	//
- (id) initWithFrame:(CGRect)frame
{
		//
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{
			//listen for the XML data to load
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//register selector to observe for notification
		[nc addObserver:self selector: @selector(doAddOutfit:) name:@"OLISelectedOutfit" object:nil];
		
			//
		UIImage *bgImage = [UIImage imageNamed: @"closetidea4"];
		
			//
		closetView = [[UIImageView alloc] initWithImage: bgImage];
		
		[self addSubview: closetView];		
    }
	
		//
    return self;
}

	//
- (void) displayPaperDoll: (OLIPaperDoll *) thePaperDoll withOutfits: (NSDictionary *) theOutfits
{
		//
	if( outfits != nil )
		[outfits release];
	
		//
	outfits = [theOutfits retain];
	
		//create a model instance of the doll
	paperDoll = [[OLIPaperDollView alloc] initWithFrame: CGRectMake( 415, 150, 295, 600)];
	
		//display the doll selected
	[paperDoll setDoll: [thePaperDoll retain]];
	
		//populate it into the display
	[self addSubview: paperDoll];	
	
		//
	hatScrolView = [[OLIOutfitScrollView alloc] initWithFrame: CGRectMake( 496, 52, 134, 114)];
	
		//
	[hatScrolView displayOutfitsWithData: [outfits objectForKey: @"Hats"] offset: -50 andSeperation: -100];

		//
	shirtsScrolView = [[OLIOutfitScrollView alloc] initWithFrame: CGRectMake( 92, 78, 220, 140)];
	
		//
	[shirtsScrolView displayOutfitsWithData: [outfits objectForKey: @"Shirts"] offset: -50 andSeperation: -100];
	
		//
	pantsScrolView = [[OLIOutfitScrollView alloc] initWithFrame: CGRectMake( 92, 200, 220, 390)];
	
		//
	[pantsScrolView displayOutfitsWithData: [outfits objectForKey: @"Pants"] offset: -50 andSeperation: -100];
	
		//
	handbagScrolView = [[OLIOutfitScrollView alloc] initWithFrame: CGRectMake( 675, 10, 325, 70)];
	
		//
	[handbagScrolView displayOutfitsWithData: [outfits objectForKey: @"Handbags"] offset: -50 andSeperation: -100];
	
		//
	dressesScrolView = [[OLIOutfitScrollView alloc] initWithFrame: CGRectMake( 780, 100, 225, 450)];
	
		//
	[dressesScrolView displayOutfitsWithData: [outfits objectForKey: @"Dresses"] offset: 0 andSeperation: -30];
	
		//
	shoesScrolView = [[OLIOutfitScrollView alloc] initWithFrame: CGRectMake( 36, 580, 280, 110)];
	
		//
	[shoesScrolView displayOutfitsWithData: [outfits objectForKey: @"Shoes"] offset: -50 andSeperation: -100];
	
		//
	[self addSubview: hatScrolView];	
	[self addSubview: shirtsScrolView];	
	[self addSubview: pantsScrolView];	
	[self addSubview: handbagScrolView];	
	[self addSubview: dressesScrolView];
	[self addSubview: shoesScrolView];
}

	//
- (void) doAddOutfit: (NSNotification *) note
{
		//get the outfit being added
	OLIOutfit *outfit = [note object];
	
		//place the outfit on the doll
	[paperDoll addOutfit: outfit];
	
		//alert to save the doll to the db
	[self savePaperDoll];
}

	// 
- (void) savePaperDoll
{
		//retreives the default notification center 
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got selected
	[nc postNotificationName: @"OLISaveDoll" object: [paperDoll doll]];
}

/*********** Deconstruction ***********/

	//
- (void) dealloc
{
		//listen for the XML data to load
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//remove this as a observer of messages
	[nc removeObserver: self];
	
		//
	[closetView release];
	
		//
	[dressesScrolView release];
	[shirtsScrolView release];
	[pantsScrolView release];
	[hatScrolView release];
	[handbagScrolView release];
	[shoesScrolView release];
	
		//
	[outfits release];
	[paperDoll release];
	
		//
    [super dealloc];
}


@end
