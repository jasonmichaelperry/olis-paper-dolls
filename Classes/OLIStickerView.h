//
//  OLIStickerView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLISticker.h"

@interface OLIStickerView : UIView
{
		//visual assets
	UIImageView *stickerImage;
	
		//
	OLISticker *sticker;
}

	//
@property( nonatomic, retain ) OLISticker *sticker;

	//
- (void) setSticker: (OLISticker *) theSticker;

@end
