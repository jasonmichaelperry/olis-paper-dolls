//
//  OLIOutfit.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIOutfit.h"


@implementation OLIOutfit

	// 
@synthesize outfitId;
@synthesize outfitName;
@synthesize outfitType;
@synthesize outfitData;
@synthesize outfitWidth;
@synthesize outfitHeight;

@end
