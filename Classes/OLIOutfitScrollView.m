//
//  OutfitScrollView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIOutfitScrollView.h"


@implementation OLIOutfitScrollView


	//
- (id) initWithFrame:(CGRect)frame
{
		//
    self = [super initWithFrame:frame];
    
		//
	if (self)
	{
			//No doll yet selected
		selectedOutfit = nil;
		
			//retains space for the doll views
		outfitViewsArray = [[NSMutableArray alloc] init];
		
			//creates a gesture to select a doll
		tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(doOutfitSelect:)];
		tapGesture.numberOfTouchesRequired = 1;	//number of taps needed for a touch
												//[tapGesture setDelaysTouchesBegan: YES];
		
			//create a gesture to swipe left though dolls
		panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(doOutfitPan:)];
		panGesture.maximumNumberOfTouches = 1;
		
			//add all gestures
		[self addGestureRecognizer: tapGesture];
		[self addGestureRecognizer: panGesture];
		
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
		[self setClipsToBounds: YES];
		[self setUserInteractionEnabled: YES];
		
			//
		outfitsView = [[UIView alloc] init];
		
			//
		[self addSubview: outfitsView];
    }
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) displayOutfitsWithData: (NSArray *) outfitData offset: (float) startX andSeperation: (float) spacing
{
		//retains the local NSArray in memory
	outfitsArray = [outfitData retain];
	
		//
	float xPos = startX;
	float yPos = 0;
	float spacer = spacing;
	
		//step through the array of dolls and show them
	for ( int i = 0; i < [outfitsArray count]; i++ )
	{
			//create a model instance of the doll
		OLIOutfit *outfit = (OLIOutfit *)[outfitsArray objectAtIndex: i];
		
			//create a view for this doll
		OLIOutfitView *outfitItemView = [[OLIOutfitView alloc] initWithFrame: CGRectMake( xPos, yPos, [outfit outfitWidth], [outfit outfitHeight] )];
		[outfitItemView setOutfit: outfit];
		
			//populate it into the display
		[outfitsView addSubview: outfitItemView];
		
			//store the view in the array
		[outfitViewsArray insertObject: outfitItemView atIndex: 0];
		
			//
		[outfitItemView release];
		
			// 
		xPos += [outfit outfitWidth] + spacer; 
	}
	
		//
	actualWidth = xPos;
}

/*********** Gesture Selectors ***********/

	//
- (void) doOutfitSelect: (UITapGestureRecognizer *) sender
{		
		//originial tap location
	CGPoint tapPoint = [sender locationInView: outfitsView];
	
		//step through each doll for the selected one
	for( OLIOutfitView *outfitItemView in outfitViewsArray )
	{
			//used to determine if is in bounds
		CGFloat xMin = outfitItemView.frame.origin.x;
		CGFloat xMax = outfitItemView.frame.origin.x + outfitItemView.frame.size.width;
		CGFloat yMin = outfitItemView.frame.origin.y;
		CGFloat yMax = outfitItemView.frame.size.height;
		
			//it is within
		if( tapPoint.x > xMin && tapPoint.x < xMax && tapPoint.y > yMin && tapPoint.y < yMax )
		{
				//get the paper doll that was selected
			selectedOutfit = outfitItemView;
			
				//no need to keep looping.  We got it.
			break;
		}
	}
	
		//if a paper doll was selected
	if( selectedOutfit != nil )
	{
			//retreives the default notification center
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//posts a notification that show dolls got selected
		[nc postNotificationName: @"OLISelectedOutfit" object: [selectedOutfit outfit]];
	}
}

	//
- (void) doOutfitPan: (UIPanGestureRecognizer *) sender
{
		//stores first point from beginning
	static CGPoint first;
	
		//to store the translation point and final point
	CGPoint transPoint = [sender translationInView: outfitsView];
	CGPoint final;
	
		//is this beginning of the pan?
	if( [sender state] == UIGestureRecognizerStateBegan )
	{
			//get the first values
		first = [outfitsView center];
	}
	
		//determine the final location
	final = CGPointMake( first.x + transPoint.x, first.y );

		//
	if( fabs(final.x) > ( actualWidth - self.frame.size.width ) )
		final.x = -( actualWidth - self.frame.size.width );
	
		//make sure we did not scroll to far
	if( final.x > 0 )
		final.x = 0;	

		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .25];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
	
		//point to animate too
	[outfitsView setCenter: CGPointMake( final.x, first.y)];
	
		//do the animation
	[UIView commitAnimations];				
}

/*********** Deconstruction ***********/

	//
- (void) dealloc
{
		//
	[self removeGestureRecognizer: tapGesture];
	[self removeGestureRecognizer: panGesture];
	
	
		//step through each doll for the selected one
	for( OLIOutfitView *outfitItemView in outfitViewsArray )
	{
			//
		[outfitItemView removeFromSuperview];
		
			//
			//[outfitItemView release];
	}
	
		//
	[outfitViewsArray release];
	[outfitsArray release];
	[outfitsView release];
	
		//
    [super dealloc];
}


@end
