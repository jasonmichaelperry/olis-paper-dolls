//
//  OLIPaperDoll.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIOutfit.h"

@interface OLIPaperDoll : NSObject
{
		//
	int dollId;
	NSString *dollName;
	NSData *dollData;
	
		//
	OLIOutfit *shoes;
	OLIOutfit *dress;
	OLIOutfit *pants;
	OLIOutfit *shirt;
	OLIOutfit *handbag;
	OLIOutfit *hat;
}

	//
@property( assign ) int dollId;
@property( nonatomic, retain ) NSString *dollName;
@property( nonatomic, retain ) NSData *dollData;

	//
@property( nonatomic, retain ) OLIOutfit *shoes;
@property( nonatomic, retain ) OLIOutfit *dress;
@property( nonatomic, retain ) OLIOutfit *pants;
@property( nonatomic, retain ) OLIOutfit *shirt;
@property( nonatomic, retain ) OLIOutfit *handbag;
@property( nonatomic, retain ) OLIOutfit *hat;

@end
