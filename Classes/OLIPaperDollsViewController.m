//
//  OliPaperDollsViewController.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPaperDollsViewController.h"

@implementation OLIPaperDollsViewController

/*********** Initiiliation and Construction ***********/

	// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void) loadView
{	
		//listen for the XML data to load
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//register selector to observe for notification
	[nc addObserver:self selector:@selector(doShowDolls:) name:@"OLIShowDolls" object:nil];
	[nc addObserver:self selector:@selector(doShowPictures:) name:@"OLIShowPictures" object:nil];
	[nc addObserver:self selector:@selector(doShowStore:) name:@"OLIShowStore" object:nil];
		 
		//the path to the sound file
	NSString *oliIntroClipPath = [[NSBundle mainBundle] pathForResource: @"introVoice" ofType: @"wav"];
	
		//the id of this sound we want to play
	SystemSoundID soundID;
	
		//Create and play our sound when the game starts
	AudioServicesCreateSystemSoundID( (CFURLRef) [NSURL fileURLWithPath: oliIntroClipPath], &soundID );
	AudioServicesPlaySystemSound( soundID );
	
		//
	timer = [NSTimer scheduledTimerWithTimeInterval: 3 target: self selector: @selector(doGameStart:) userInfo: nil repeats: NO];
	
		//
	UIImage *bgImage = [UIImage imageNamed: @"mainbackgroundidea2"];
	
		//
	backgroundView = [[UIImageView alloc] initWithImage: bgImage ];
		
		//create the intro view for display
	introView = [[OLIPaperDollIntro alloc] initWithFrame: mainContent.frame];

		//
	overlay = [[OLIMainMenuView alloc] initWithFrame: CGRectMake( 725, 650, 300, 100)];
	
		//
	mainContent = [[UIView alloc] initWithFrame: CGRectMake( 0, 0, 1024, 748 )];
	
		//
		//[mainContent setBackgroundColor: [UIColor scrollViewTexturedBackgroundColor]];
	[mainContent addSubview: backgroundView];
	[mainContent addSubview: overlay];
	[mainContent addSubview: introView];
	
		//add the main content view to the screen
	self.view = mainContent;
	
		//
		//[oliIntroClipPath release];
}


	// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
		//
    [super viewDidLoad];
}

/*********** Helper Delegates ***********/

	// Override to allow orientations other than the default portrait orientation.
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
		//
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

	//
- (void) didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


/*********** Timer Countdown Targets ***********/


	//
- (void) doGameStart: (NSTimer *) theTimer
{
		//
	[self doShowDolls: nil];
}


/*********** Button and Notification Targets ***********/

	//
- (void) doShowDolls: (id) sender
{
		//if the controller is there alredy remove it
	if( myDollsController != nil )
	{
			//remove it from the display now
		[myDollsController doShowAllDolls];
		
			//
		return;
	}
	
		//
	if(  myPicturesController != nil )
	{
		[myPicturesController animateClose];
	}
	
		//
	if(  myShopPacksController != nil )
	{
		[myShopPacksController animateClose];
	}	
	
		//create a new controller for my dolls
	myDollsController = [[OLIMyPaperDollsViewController alloc] init];
		
		//prepopulate with paper doll
	[myDollsController doShowAllDolls];
		
		//display the controller's view
	[mainContent insertSubview: myDollsController.view atIndex: 1];

		//
    [self performSelector: @selector(doShowDollsComplete)
			   withObject: nil
			   afterDelay: .7
	 ];
}

- (void) doShowDollsComplete
{
		//if not already removed 
	if( introView != nil )
	{
			//remove the intro view from display
		[introView removeFromSuperview];
		
			//release the intro view
		[introView release];

			//nil out the instance
		introView = nil;
	}
	
		//also attempt to remove the pictures view
	if( myPicturesController != nil )
	{
			//
		[myPicturesController.view removeFromSuperview];
		
			//
		[myPicturesController release];
		
			//
		myPicturesController = nil;
	}
	
		//
	if( myShopPacksController != nil )
	{
			//
		[myShopPacksController.view removeFromSuperview];
		
			//
		[myShopPacksController release];
		
			//
		myShopPacksController = nil;
	}
}

	//
- (void) doShowPictures: (id) sender
{
		//if the controller is there alredy remove it
	if( myPicturesController != nil )
	{
			//remove it from the display now
		[myPicturesController doShowAllPictures];
		
			//
		return;
	}
	
		//
	if(  myDollsController != nil )
	{
		[myDollsController animateClose];
	}
	
		//
	if(  myShopPacksController != nil )
	{
		[myShopPacksController animateClose];
	}
	
		//create a new controller for my dolls
	myPicturesController = [[OLIMyPicturesViewController alloc] init];
		
		//
	[myPicturesController doShowAllPictures];
	
		//display the controller's view
	[mainContent insertSubview: myPicturesController.view atIndex: 1];
	
		//
    [self performSelector: @selector(doShowPicturesComplete)
			   withObject: nil
			   afterDelay: .7
	 ];
}

	//
- (void) doShowPicturesComplete
{
		//if not already removed 
	if( introView != nil )
	{
			//remove the intro view from display
		[introView removeFromSuperview];
		
			//release the intro view
		[introView release];
		
			//nil out the instance
		introView = nil;
	}
	
		//also attempt to remove the dolls view
	if( myDollsController != nil )
	{
			//
		[myDollsController.view removeFromSuperview];
		
			//
		[myDollsController release];
		
			//
		myDollsController = nil;
	}
	
		//
	if( myShopPacksController != nil )
	{
			//
		[myShopPacksController.view removeFromSuperview];
		
			//
		[myShopPacksController release];
		
			//
		myShopPacksController = nil;
	}
}

	//
- (void) doShowStore: (id) sender
{
		//if the controller is there alredy remove it
	if( myShopPacksController != nil )
	{
			//remove it from the display now
			//[myShopPacksController doShowAllPictures];
		
			//
		return;
	}
	
		//
	if(  myDollsController != nil )
	{
		[myDollsController animateClose];
	}

		//
	if(  myPicturesController != nil )
	{
		[myPicturesController animateClose];
	}
	
		//create a new controller for my dolls
	myShopPacksController = [[OLIShopPacksViewController alloc] init];
	
		//
		//[myShopPacksController doShowAllPictures];
	
		//display the controller's view
	[mainContent insertSubview: myShopPacksController.view atIndex: 1];
	
		//
    [self performSelector: @selector(doShowStoreComplete)
			   withObject: nil
			   afterDelay: .7
	 ];
}

- (void) doShowStoreComplete
{
		//if not already removed 
	if( introView != nil )
	{
			//remove the intro view from display
		[introView removeFromSuperview];
		
			//release the intro view
		[introView release];
		
			//nil out the instance
		introView = nil;
	}
	
		//also attempt to remove the pictures view
	if( myPicturesController != nil )
	{
			//
		[myPicturesController.view removeFromSuperview];
		
			//
		[myPicturesController release];
		
			//
		myPicturesController = nil;
	}
	
		//
	if( myShopPacksController != nil )
	{
			//
		[myShopPacksController.view removeFromSuperview];
		
			//
		[myShopPacksController release];
		
			//
		myShopPacksController = nil;
	}
}


/*********** Deconstruction ***********/

	//
- (void) viewDidUnload
{
		// Release any retained subviews of the main view.
		// e.g. self.myOutlet = nil;
}

	//
- (void) dealloc
{
		//listen for the XML data to load
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//remove this as a observer of messages
	[nc removeObserver: self];	
		
		//
	[backgroundView release];
	[introView release];
	[timer release];
	
		//
    [super dealloc];
}

@end
