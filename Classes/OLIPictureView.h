//
//  OLIPictureView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIPicture.h"

@interface OLIPictureView : UIView 
{
		//visual assets
	UIImageView *pictureImage;
	
		//
	OLIPicture *picture;
}

	//
@property( nonatomic, retain ) OLIPicture *picture;

	//
- (void) setPicture: (OLIPicture *) thePicture;

@end
