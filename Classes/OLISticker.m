//
//  OLISticker.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLISticker.h"


@implementation OLISticker

	// 
@synthesize stickerId;
@synthesize stickerName;
@synthesize stickerType;
@synthesize stickerData;
@synthesize stickerWidth;
@synthesize stickerHeight;

@end
