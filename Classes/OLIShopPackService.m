//
//  OLIShopPackService.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/26/11.
//  Copyright 2011 Jason Michael Perry/Smart Ameba. All rights reserved.
//

#import "OLIShopPackService.h"


@implementation OLIShopPackService

	//
static OLIShopPackService* _instance = nil;

	//
+ (OLIShopPackService*) instance
{
		//
	@synchronized([OLIShopPackService class])
	{
			//
		if (!_instance)
		{
				//
			[[self alloc] init];			
		}
		
			//
		return _instance;
	}
	
	return nil;
}

	//
+ (id) alloc
{
		//
	@synchronized([OLIShopPackService class])
	{
			//
		NSAssert(_instance == nil, @"Attempted to allocate a second instance of a singleton.");
		
			//
		_instance = [super alloc];
		
			//
		return _instance;
	}
	
		//
	return nil;
}

	//Init the Service and DB access
- (id) init
{
		//
	self = [super init];
	
		//
	if( self )
	{
	}
	
		//
	return self;
	
}

@end
