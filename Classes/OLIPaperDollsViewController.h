//
//  OliPaperDollsViewController.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "OLIMyPaperDollsViewController.h"
#import "OLIMyPicturesViewController.h"
#import "OLIShopPacksViewController.h"
#import "OLIMainMenuView.h"
#import "OLIPaperDollIntro.h"
#import "OLIPaperDollView.h"
#import "OLIPaperDoll.h"
#import "OLIOutfit.h"

@interface OLIPaperDollsViewController : UIViewController
{
		//
	NSTimer *timer;
		
		//UI visual assets
	UIView *mainContent;	//stores the viewable content region -- used to all this view to control global actions
	UIImageView *backgroundView;
	
	
		//the first screen displayed
	OLIPaperDollIntro *introView;	//the initial intro view shown to users
		//
	OLIMainMenuView *overlay;
	
		//Core view controllers for the app sections
	OLIMyPaperDollsViewController *myDollsController;	//view controller for creating and customizing a paper doll
	OLIMyPicturesViewController *myPicturesController;	//view controller for creating and customizing a picture and scene	
	OLIShopPacksViewController *myShopPacksController;	//view controller for buying add on outfit packs
}
	//
- (void) doShowDolls: (id) sender;
- (void) doShowPictures: (id) sender;

@end

