    //
//  OLIShopPacksViewController.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/26/11.
//  Copyright 2011 Jason Michael Perry/Smart Ameba. All rights reserved.
//

#import "OLIShopPacksViewController.h"


@implementation OLIShopPacksViewController

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{

}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
		//
    NSMutableArray *productDetailsList    = [[NSMutableArray alloc] init];  
    NSMutableArray *productIdentifierList = [[NSMutableArray alloc] init];  
	
		//
    for (short item_count=1; item_count <= 3; item_count++)
	{  
        [productIdentifierList addObject:[NSString stringWithFormat:@"com.mobiletuts.inappdemo.%d", item_count]];  
    }
	
		//
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:productIdentifierList]];  
	
		//
    request.delegate = self; 
		
		//
    [super viewDidLoad];
}

/*********** Helper Delegates ***********/

	//
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
		// Overriden to allow any orientation.
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

	//
- (void)didReceiveMemoryWarning
{
		// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
		// Release any cached data, images, etc. that aren't in use.
}

/*********** Store Kit Delegates ***********/

	//return of App Store products
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response  
{  
		//
		//[productDetailsList addObjectsFromArray: response.products];  
		//[productDisplayTableView reloadData];  
}  

	//alerts that our request finished as expected
-(void)requestDidFinish:(SKRequest *)request  
{  
		//
    [request release];  
}  

	//The call to the server failed, prob due to lack of a connection
-(void)request:(SKRequest *)request didFailWithError:(NSError *)error  
{  
		//
    NSLog(@"Failed to connect with error: %@", [error localizedDescription]);  
}  


/*********** Animations ***********/

	//
- (void) animateClose
{
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .50];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(doShowAllPicturesComplete:)];
	
		//point to animate too
	[self.view setTransform: CGAffineTransformMakeTranslation(0, 768)];
	
		//do the animation
	[UIView commitAnimations];
}

/*********** Deconstruction ***********/

	//
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

	//
- (void)dealloc
{
    [super dealloc];
}


@end
