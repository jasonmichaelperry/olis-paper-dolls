//
//  OLIStickerScrollView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLIStickerView.h"

@interface OLIStickerOverlayScrollView : UIView
{
		//
	float actualWidth;

		//data storage bins
	NSArray *stickersArray;
	
		//array of all dolls
	NSMutableArray *stickerViewsArray;
	
		//visual controls
	UIView *stickersView;	//large view contains all of the dolls
	
		//the currently selected doll
	OLIStickerView *selectedSticker;
	
		//
	UITapGestureRecognizer *tapGesture;
	UIPanGestureRecognizer *panGesture;	
}

- (void) displayStickersWithData: (NSArray *) stickerData forCategory: (NSString *) categoryName;

	//
- (float) sizeToHeight: (float) toHeight from: (CGSize) fromSize;
- (float) sizeToWidth: (float) theWidth from: (CGSize) fromSize;


@end
