//
//  OLIPaperDollSelectorView.h
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "OLIPaperDollView.h"
#import "OLIPaperDoll.h"

@interface OLIPaperDollScrollView : UIView
{
		//
	float actualWidth;
	
		//data storage bins
	NSArray *paperDollArray;
	
		//array of all dolls
	NSMutableArray *paperDollViews;
	
		//visual controls
	UIView *dollsView;	//large view contains all of the dolls
	
		//the currently selected doll
	OLIPaperDollView *selectedPaperDoll;
	
		//
	UITapGestureRecognizer *tapGesture;
	UIPanGestureRecognizer *panGesture;
	
		//
    UIViewController *parentController;
}

	// Don't use retain or you'll have a circular reference
@property(nonatomic, assign) UIViewController *parentController;

	//
- (void) displayPaperDollsWithData: (NSArray *) dollData;
- (void) displayPaperDollThumbnailsWithData: (NSArray *) dollData;
- (void) displayPaperDollsWithX: (float) theX andY: (float) theY forWidth: (float) theWidth andHeight: (float) theHeight;

	//
- (void) playDollSelectSound;

	//
- (float) sizeToHeight: (float) toHeight from: (CGSize) fromSize;
- (float) sizeToWidth: (float) theWidth from: (CGSize) fromSize;

@end
