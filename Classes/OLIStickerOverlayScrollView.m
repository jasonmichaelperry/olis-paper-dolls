//
//  OLIStickerScrollView.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIStickerOverlayScrollView.h"


@implementation OLIStickerOverlayScrollView


- (id)initWithFrame:(CGRect)frame 
{
		//
    self = [super initWithFrame:frame];
	
		//
    if (self)
	{
			//No doll yet selected
		selectedSticker = nil;
		
			//retains space for the doll views
		stickerViewsArray = [[NSMutableArray alloc] init];
				
			//creates a gesture to select a doll
		tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(doStickerSelect:)];
		tapGesture.numberOfTouchesRequired = 1;	//number of taps needed for a touch
		
			//create a gesture to swipe left though dolls
		panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(doStickerPan:)];
		panGesture.maximumNumberOfTouches = 1;
		
			//add all gestures
		[self addGestureRecognizer: tapGesture];
		[self addGestureRecognizer: panGesture];
		
			//maintain a clear background
		[self setBackgroundColor: [UIColor clearColor]];
		[self setClipsToBounds: YES];
		[self setUserInteractionEnabled: YES];
				
			//
		stickersView = [[UIView alloc] initWithFrame: CGRectMake( 0, 20, 600, 200 )];
		
			//
		[self addSubview: stickersView];
    }
	
		//
    return self;
}

/*********** Data Population ***********/

	//
- (void) displayStickersWithData: (NSArray *) stickerData forCategory: (NSString *) categoryName
{
		//retains the local NSArray in memory
	stickersArray = [stickerData retain];
		
		//
	float xPos = 0;
	float yPos = 0;
	float height = 200;
	float width = 200;
	float spacer = 0;
	
		//step through the array of dolls and show them
	for ( int i = 0; i < [stickersArray count]; i++ )
	{
			//create a model instance of the doll
		OLISticker *sticker = (OLISticker *)[stickersArray objectAtIndex: i];
		
			//determine the width by aspect ratio
		width = [self sizeToHeight: height from: CGSizeMake( [sticker stickerWidth], [sticker stickerHeight] )];

			//create a view for this doll
		OLIStickerView *stickerItemView = [[OLIStickerView alloc] initWithFrame: CGRectMake( xPos, yPos, width, height )];
		[stickerItemView setSticker: sticker];
		
			//populate it into the display
		[stickersView addSubview: stickerItemView];
		
			//store the view in the array
		[stickerViewsArray addObject: stickerItemView];
		
			//
		[stickerItemView release];
		
			//
		xPos += width + spacer;
	}
	
		//
	actualWidth = xPos;
}

/*********** Gesture Selectors ***********/

	//
- (void) doStickerSelect: (UITapGestureRecognizer *) sender
{		
		//originial tap location
	CGPoint tapPoint = [sender locationInView: stickersView];
	
		//step through each doll for the selected one
	for( OLIStickerView *stickerItemView in stickerViewsArray )
	{
			//used to determine if is in bounds
		CGFloat xMin = stickerItemView.frame.origin.x;
		CGFloat xMax = stickerItemView.frame.origin.x + stickerItemView.frame.size.width;
		CGFloat yMin = stickerItemView.frame.origin.y;
		CGFloat yMax = stickerItemView.frame.origin.y + stickerItemView.frame.size.height;
		
			//it is within
		if( tapPoint.x > xMin && tapPoint.x < xMax && tapPoint.y > yMin && tapPoint.y < yMax )
		{
				//get the paper doll that was selected
			selectedSticker = stickerItemView;
			
				//no need to keep looping.  We got it.
			break;
		}
	}
	
		//if a paper doll was selected
	if( selectedSticker != nil )
	{
			//retreives the default notification center
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		
			//posts a notification that show dolls got selected
		[nc postNotificationName: @"OLISelectedSticker" object: [selectedSticker sticker]];
	}
}

	//
- (void) doStickerPan: (UIPanGestureRecognizer *) sender
{
		//stores first point from beginning
	static CGPoint first;
	
		//to store the translation point and final point
	CGPoint transPoint = [sender translationInView: stickersView];
	CGPoint final;
	
		//is this beginning of the pan?
	if( [sender state] == UIGestureRecognizerStateBegan )
	{
			//get the first values
		first = [stickersView center];
	}
	
		//determine the final location
	final = CGPointMake( first.x + transPoint.x, first.y );
	
		//
	if( fabs(final.x) > ( actualWidth - self.frame.size.width ) )
		final.x = -( actualWidth - self.frame.size.width );
	
		//make sure we did not scroll to far
	if( final.x > 0 )
		final.x = 0;		
	
		//animate
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: .25];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
	
		//point to animate too
	[stickersView setCenter: CGPointMake( final.x, first.y)];
	
		//do the animation
	[UIView commitAnimations];				
}

	// 
- (void) doClose: (UIButton *) sender
{
		//retreives the default notification center
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
		//posts a notification that show dolls got selected
	[nc postNotificationName: @"OLIStickerClose" object: nil];	
}

/*********** Helper Methods ***********/

	//
- (float) sizeToHeight: (float) toHeight from: (CGSize) fromSize
{
		//original height / original width x new width = new height
	return (fromSize.width/fromSize.height) * toHeight;
}


	// 
- (float) sizeToWidth: (float) theWidth from: (CGSize) fromSize
{
		//original height / original width x new width = new height
	return (fromSize.height/fromSize.width) * actualWidth;
}



/*********** Deconstruction ***********/

- (void)dealloc
{
		//step through each doll for the selected one
	for( OLIStickerView *stickerItemView in stickerViewsArray )
	{
			//
		[stickerItemView removeFromSuperview];		
	}
	
		//
	[self removeGestureRecognizer: tapGesture];
	[self removeGestureRecognizer: panGesture];	
	
		//
	[stickerViewsArray release];
	
		//	
    [super dealloc];
}


@end
