//
//  OLIPaperDollIntro.m
//  OliPaperDolls
//
//  Created by Jason Michael Perry on 2/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OLIPaperDollIntro.h"


@implementation OLIPaperDollIntro

/*********** Initilization and Construction ***********/

	// 
- (id)initWithFrame:(CGRect)frame
{
		//
    self = [super initWithFrame:frame];
	
		//
    if (self)
	{
			//
		UIImage *introImage = [UIImage imageNamed: @"IntroScreen"];
		
			//
		introImageView = [[UIImageView alloc] initWithImage: introImage];
		
			//add the views to the display
		[self addSubview: introImageView];
		
			//
			//[introImage release];
    }
	
		//
    return self;
}

/*********** Deconstruction ***********/

	//
- (void)dealloc
{
		//
	[introImageView release];
	
		//
    [super dealloc];
}


@end
